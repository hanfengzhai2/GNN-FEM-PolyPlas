import os
import pickle
import torch
from torch_geometric.data import Data
import networkx as nx
import numpy as np
cur_dir = os.getcwd()

def assign_stress_strain_to_graph(graph, stress_data, strain_data):
    for i, stress in enumerate(stress_data[0]):
        graph.nodes[i]['stress'] = stress
    for i, strain in enumerate(strain_data[0]):
        graph.nodes[i]['strain'] = strain * 1e4

def graph2torch(graph):
    node_features = []
    target_features = []

    for _, node_data in graph.nodes(data=True):
        stress = node_data.get('stress', np.zeros(6))  
        strain = node_data.get('strain', np.zeros(6))  

        node_features.append(strain)
        target_features.append(stress)

    node_features = torch.tensor(node_features, dtype=torch.float)
    target_features = torch.tensor(target_features, dtype=torch.float)

    edge_index = []; edge_attr = []

    for i, j, edge_data in graph.edges(data=True):
        edge_index.append([i, j])
        edge_attr.append(edge_data.get('weight') * 1e3)  # Default weight to 1.0

    edge_index = torch.tensor(edge_index, dtype=torch.long).t().contiguous()
    edge_attr = torch.tensor(edge_attr, dtype=torch.float)

    data = Data(x=node_features, edge_index=edge_index, edge_attr=edge_attr, y=target_features)

    return data

with open(f"{cur_dir}/example_dat/reordered_stress_strain_data.pkl", 'rb') as f:
    reordered_stress, reordered_strain = pickle.load(f)

with open(f"{cur_dir}/example_dat/reordered_all_graphs.pkl", 'rb') as f:
    reordered_graphs = pickle.load(f)

graph_tensors = []
for i in range(len(reordered_graphs)):
    graph = reordered_graphs[i]
    stress_data = reordered_stress[i]
    strain_data = reordered_strain[i]
    
    assign_stress_strain_to_graph(graph, stress_data, strain_data)
    
    graph_tensor = graph2torch(graph)
    graph_tensors.append(graph_tensor)

os.makedirs('out/', exist_ok=True)
with open("{cur_dir}/out/all_graphs.pkl", 'wb') as f:
    pickle.dump(graph_tensors, f)

print(f"Graph tensors have been created and saved: {graph_tensors[:5]} ...")