#!/bin/bash
#SBATCH --job-name=plas_gnn
#SBATCH --output=plas_gnn.out
#SBATCH --error=plas_gnn.err
#SBATCH --time=30:00:00
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=32
#SBATCH --mem-per-cpu=2G

echo "Load the corresponding modules in your cluster"
# *** module load python/3.9
# *** module load gmsh
# *** module load gcc/12.1.
# *** module load gsl
# *** module load cmake
export neper=/your/path/to/neper/src/build/neper

format_time() {
    local total_seconds=$1
    local hours=$((total_seconds / 3600))
    local minutes=$(( (total_seconds % 3600) / 60 ))
    local seconds=$((total_seconds % 60))
    printf "%02d:%02d:%02d" $hours $minutes $seconds
}

total_time=0
iterations=0

for a in {1..900}
do
    rm -rf simulation.sim
    rm simulation* 
    rm *.tess
    rm *.msh
    cd sim_results_test; mkdir "result_${a}/"; cd ..; pwd    

    # *** module load gcc/12.1.
    # *** module load gsl

    num_grains=10

    $neper -T -n $num_grains -periodicity all -id $a
    $neper -M n${num_grains}-id${a}.tess

    mv *.msh simulation.msh
    mv *.tess simulation.tess

    # *** module load openmpi
    cp /your/path/to/periodic/simulation.cfg ./
    echo "START SIM"
    t_start=$(date +%s)  # Capture start time in seconds since epoch

    /your/path/to/fepx/src/build/fepx &  # Run the simulation in the background
    sim_pid=$!  # Capture the process ID of the simulation

    # Monitor the simulation
    while kill -0 $sim_pid 2>/dev/null; do
        t_now=$(date +%s)  # Capture current time
        elapsed=$((t_now - t_start))  # Calculate elapsed time
        if [ $elapsed -gt 6000 ]; then  # Check if elapsed time is greater than 1800 seconds (30 minutes)
            echo "Simulation $a is taking too long. Terminating..."
            kill -9 $sim_pid  # Forcefully terminate the simulation
            break  # Exit the while loop and proceed to the next iteration
        fi
        sleep 60  # Check every minute
    done

    t_end=$(date +%s)  # Capture end time
    iteration_time=$((t_end - t_start))  # Calculate iteration time
    total_time=$((total_time + iteration_time))  # Accumulate total time
    iterations=$((iterations + 1))  # Increment iteration count

    echo "Simulation $a completed in $(format_time $iteration_time)"
    # Check if the simulation completed within the time limit
    if [ $((t_end - t_start)) -le 6000 ]; then
        cp -r simulation.sim/ /home/users/hzhai/codes/fem_plas/sim_results_test/"result_${a}/"
    fi

    cp -r simulation.sim/ /your/path/to/sim_results_test/"result_${a}/"

    average_time=$((total_time / iterations))
    remaining_iterations=$((900 - iterations))
    estimated_remaining_time=$((remaining_iterations * average_time))

    echo "Average time per calculation: $(format_time $average_time)"
    echo "Estimated remaining time: $(format_time $estimated_remaining_time)"

done