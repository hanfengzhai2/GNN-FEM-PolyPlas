import os
import numpy as np
import pickle

cur_dir = os.getcwd()
base_directory = f'{cur_dir}/sim_results/'

sim_results_dirs = base_directory
interest_folders = ['stress', 'strain']
file_name = 'step1'
sim_results_path = base_directory
stress = []; strain = []

for result_dir in os.listdir(sim_results_path):
    if result_dir.startswith('result_'):
        result_path = os.path.join(sim_results_path, result_dir)
        print(f'processing {result_path}...')
        stress_data = []; strain_data = []

        for folder in interest_folders:
            file_path = os.path.join(result_path, 'simulation.sim/results', 'elts', folder, f'{folder}.{file_name}')
                
            with open(file_path, 'r') as f:
                data = f.readlines()
                data = [list(map(float, line.split())) for line in data]
                data_array = np.array(data)
                    
            if folder == 'stress':
                stress_data.append(data_array)
            elif folder == 'strain':
                strain_data.append(data_array)
            
        stress.append(stress_data)
        strain.append(strain_data)

if stress:
    print("Example data from stress[0][0]:")
    print(stress[0][0][:5])  # Print first 5 rows of the first stress array

with open(f'{cur_dir}/example_dat/reordered_stress_strain.pkl', 'wb') as f:
    pickle.dump((stress, strain), f)

print("Data extraction and processing completed.")