import numpy as np
from scipy.stats import pearsonr
from sklearn.metrics import r2_score, mean_absolute_error
from matplotlib.patches import Patch

def read_stress_bench(prefix):
    components = []
    for i in range(1, 7):
        file_path = f"{prefix}{i}.bench"
        components.append(np.loadtxt(file_path))
    return components

def read_stress_pred(prefix):
    components = []
    for i in range(1, 7):
        file_path = f"{prefix}{i}.pred"
        components.append(np.loadtxt(file_path))
    return components


def calculate_von_mises(sig1, sig2, sig3, sig4, sig5, sig6):
    return np.sqrt(0.5 * ((sig1 - sig2)**2 + (sig4 - sig6)**2 + (sig6 - sig1)**2 + 6 * (sig5**2 + sig3**2 + sig2**2)))

def log_transform(data):
    return np.log1p(data)  # log1p is used to handle zero values

sig1_bench, sig2_bench, sig3_bench, sig4_bench, sig5_bench, sig6_bench = read_stress_bench('stress')
sig1_pred, sig2_pred, sig3_pred, sig4_pred, sig5_pred, sig6_pred = read_stress_pred('stress')

# Calculate von Mises stress for benchmark and predicted data
stressvM_bench = calculate_von_mises(sig1_bench, sig2_bench, sig3_bench, sig4_bench, sig5_bench, sig6_bench)
stressvM_pred = calculate_von_mises(sig1_pred, sig2_pred, sig3_pred, sig4_pred, sig5_pred, sig6_pred)
stressvM_err = np.abs(stressvM_bench - stressvM_pred)

r2 = r2_score(stressvM_bench, stressvM_pred)
pearson_corr, _ = pearsonr(stressvM_bench, stressvM_pred)
mae = mean_absolute_error(stressvM_bench, stressvM_pred)


import matplotlib.pyplot as plt
from matplotlib.gridspec import GridSpec

fig = plt.figure(figsize=(5, 5))
gs = GridSpec(4, 4, figure=fig)

# Scatter plot in the joint axis
ax_joint = fig.add_subplot(gs[1:4, 0:3])
ax_joint.scatter(stressvM_bench, stressvM_pred, marker='o', alpha=0.5, color="blue")
ax_joint.plot([min(stressvM_bench), max(stressvM_bench)], [min(stressvM_pred), max(stressvM_pred)], 'r-.', label='Benchmark = Predictions')
ax_joint.set_xlabel('FEM von Mises stress solutions')
ax_joint.set_ylabel('GNN von Mises stress predictions')
ax_joint.set_title('Comparison of FEM vs. GNN')
ax_joint.legend()
ax_joint.grid(True)

# Histogram on the top marginal axis
ax_marg_x = fig.add_subplot(gs[0, 0:3], sharex=ax_joint)
ax_marg_x.hist(stressvM_bench, bins=99, orientation="vertical", alpha=0.5)
ax_marg_x.set_ylabel('Frequency')

# Histogram on the right marginal axis
ax_marg_y = fig.add_subplot(gs[1:4, 3], sharey=ax_joint)
ax_marg_y.hist(stressvM_pred, bins=99, orientation="horizontal", alpha=0.5)
ax_marg_y.set_xlabel('Frequency')

# Remove tick labels on marginals
plt.setp(ax_marg_x.get_xticklabels(), visible=False)
plt.setp(ax_marg_y.get_yticklabels(), visible=False)

# Adjust layout
fig.tight_layout()

textstr = f'$R^2 = {r2:.2f}$\nPearson = {pearson_corr:.2f}\nMAE = {mae:.2f}'
props = dict(boxstyle='round', facecolor='wheat', alpha=0.5)
ax_joint.text(0.05, 0.75, textstr, transform=ax_joint.transAxes, fontsize=10,
        verticalalignment='top', bbox=props)

plt.savefig("vonMises",dpi=300); plt.close()


stress_bench = [sig1_bench, sig2_bench, sig3_bench, sig4_bench, sig5_bench, sig6_bench]
colors_bench = plt.cm.rainbow(np.linspace(0, 1, len(stress_bench)))
stress_pred = [sig1_pred, sig2_pred, sig3_pred, sig4_pred, sig5_pred, sig6_pred]
colors_pred = plt.cm.jet(np.linspace(0, 1, len(stress_pred)))
# colors_bench = ['blue', 'red', 'orange']               # Example: replace with actual colors
# colors_pred = ['cyan', 'pink', 'yellow']                # Example: replace with actual colors
# colors_pred = colors_bench

# fig, axs = plt.subplots(nrows=1, ncols=2, figsize=(9, 3.5))

# for i in range(3):
#     axs[0].hist(stress_bench[i], bins=99, alpha=0.75, color=colors_bench[i], label=f'$\sigma_{i+1}$ (FEM)', edgecolor='black', linestyle='-.')
#     axs[0].hist(stress_pred[i], bins=99, alpha=0.5, color=colors_pred[i], label=f'$\sigma_{i+1}$ (GNN)', edgecolor='black', linestyle=':')
# axs[0].set_xlabel('Stress Values')
# axs[0].set_xscale('log')
# axs[0].set_ylabel('Frequency')
# axs[0].set_title(f'sig{i+1}_bench Histogram')
# axs[0].legend()

# for i in range(3, 6):
#     axs[1].hist(stress_bench[i], bins=99, alpha=0.5, color=colors_bench[i-3], label=f'$\sigma_{i+1}$ (FEM)', edgecolor='black', linestyle='-.')
#     axs[1].hist(stress_pred[i], bins=99, alpha=0.5, color=colors_pred[i-3], label=f'$\sigma_{i+1}$ (GNN)', edgecolor='black', linestyle=':')
# axs[1].set_yscale('log')
# axs[1].set_xlabel('Stress Values')
# axs[1].set_title(f'sig{i+1}_bench Histogram')
# axs[1].legend()
# fig = plt.figure(figsize=(10, 5))
# ax = fig.add_subplot(111, projection='3d')

def add_labels(ax, xpos, ypos, dz, zpos):
    for x, y, z, dz_val in zip(xpos, ypos, zpos, dz):
        ax.text(x + 0.1, y, z + dz_val, f'{dz_val:.2f}', color='black')


fig = plt.figure(figsize=(9, 3.5))
axs = [fig.add_subplot(121, projection='3d'), fig.add_subplot(122, projection='3d')]

for i, data in enumerate(stress_bench):
    hist, bins = np.histogram(data, bins=99)
    hist = hist / hist.max()

    xpos = bins[:-1]
    ypos = np.full_like(xpos, i)
    zpos = np.zeros_like(xpos)
    
    dx = np.diff(bins)
    dy = np.full_like(xpos, .1)
    dz = hist

    bars = axs[0].bar3d(xpos, ypos, zpos, dx, dy, dz, color=colors_bench[i], alpha=0.5)
    axs[0].set_xlabel("$\sigma_i$")
    # axs[0].set_zlabel("Normalized frequency")

for i, data in enumerate(stress_pred):
    hist, bins = np.histogram(data, bins=99)
    hist = hist / hist.max()

    xpos = bins[:-1]
    ypos = np.full_like(xpos, i)
    zpos = np.zeros_like(xpos)
    
    dx = np.diff(bins)
    dy = np.full_like(xpos, .1)
    dz = hist

    bars = axs[1].bar3d(xpos, ypos, zpos, dx, dy, dz, color=colors_bench[i], alpha=0.5)
    axs[1].set_xlabel("$\sigma_i$")
    axs[1].set_zlabel("Normalized Frequency")

legend_patches_bench = [Patch(color=color, label=f'$\\sigma_{i+1}$ (FEM)') for i, color in enumerate(colors_bench)]
legend_patches_pred = [Patch(color=color, label=f'$\\sigma_{i+1}$ (GNN)') for i, color in enumerate(colors_pred)]

# axs[0].legend(handles=legend_patches_bench, loc='upper right')
# axs[1].legend(handles=legend_patches_pred, loc='upper right')

for ax in axs:
    ax.set_yticks(np.arange(6))
    ax.set_yticklabels([f'$\\sigma_{i+1}$' for i in range(6)])

plt.tight_layout()

# plt.xlabel('Stress Values')
# plt.ylabel('Frequency')
# plt.title('Histograms of Benchmark Stress Components')
# plt.legend()
plt.savefig("hist_stress",dpi=300)


# Write the von Mises stress to new files
np.savetxt('stressvM.bench', stressvM_bench)
np.savetxt('stressvM.pred', stressvM_pred)
np.savetxt('stressvM.err', stressvM_err)



print("von Mises stress calculated and saved to 'stressvM.bench' and 'stressvM.pred'.")