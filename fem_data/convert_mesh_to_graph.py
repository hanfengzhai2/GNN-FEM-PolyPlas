import os
import meshio
import networkx as nx
import pickle
import numpy as np
from matplotlib import pyplot as plt

def calculate_centroid(tetra, points):
    return np.mean(points[tetra], axis=0)

def create_graph_with_centroids(points, cells):
    G = nx.Graph()

    if 'tetra10' not in cells:
        raise ValueError("No tetra10 cells found in the mesh.")

    tetra_cells = cells['tetra10']
    centroids = []
    
    for cell in tetra_cells:
        centroid = calculate_centroid(cell, points)
        centroids.append(centroid)

    centroids = np.array(centroids)

    for i, centroid in enumerate(centroids):
        G.add_node(i, pos=(centroid[0], centroid[1], centroid[2]))

    for i, cell1 in enumerate(tetra_cells):
        for j in range(i + 1, len(tetra_cells)):
            cell2 = tetra_cells[j]
            if len(set(cell1) & set(cell2)) == 3:  
                centroid1 = centroids[i]
                centroid2 = centroids[j]
                distance = np.linalg.norm(centroid1 - centroid2)
                G.add_edge(i, j, weight=distance)
    return G

cur_dir = os.getcwd()
base_directory = f'{cur_dir}/sim_results/'

graphs, i = [], 0
figures_directory = os.path.join(base_directory, '../fig')  

os.makedirs(figures_directory, exist_ok=True)

for result_dir in os.listdir(base_directory):
    if result_dir.startswith('result_'):
        result_path = os.path.join(base_directory, result_dir)
        print(f'processing {result_dir}')
            
        simulation_msh_path = os.path.join(result_path, 'simulation.sim/inputs', 'simulation.msh')

        mesh = meshio.read(simulation_msh_path)
        points = mesh.points
        cells = mesh.cells_dict

        G_centroids = create_graph_with_centroids(points, cells)
        graphs.append(G_centroids)

        fig_path = os.path.join(figures_directory, f'graph_{result_dir}.png')

        pos = nx.get_node_attributes(G_centroids, 'pos')
        plt.figure(figsize=(8, 6))
        nx.draw(G_centroids, node_size=10,
                node_color='blue', edge_color='gray', alpha=0.5)
        i+=1
        plt.title(f'Graph {i}')
        plt.savefig(fig_path)
        plt.close()

graphs_file_path = os.path.join(base_directory, f'{cur_dir}/example_dat/reordered_all_graphs.pkl')

with open(graphs_file_path, 'wb') as f:
    pickle.dump(graphs, f)

print(f"Generated {len(graphs)} graphs and saved corresponding figures in {figures_directory}.")