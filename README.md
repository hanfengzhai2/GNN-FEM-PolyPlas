# Learning polycrystal plasticity with Graph Neural Networks

* [arXiv:2409.05169](https://arxiv.org/abs/2409.05169)

<img src="doc/gnn_fem_schematic.png" alt="schematic for this study" width="550">

Hanfeng Zhai (`hzhai AT stanford DOT edu`)

*Department of Mechanical Engineering, Stanford University*

## Environment setup

It is recommended to use conda virtual environment. You may also directly install the following packages to implement the proposed GNN via `pip`.

```pip install -r requirement.txt```

For reproducibility, the python version used in the study:
```
$ python3 -V
Python 3.7.12
```

* You may go to **Predictions** to directly explore the pregenerated graphs and test the ML algorithms proposed in the paper.


## Data

We begin with running the FEM simulations (`run_periodic.sh`), converting the mesh to graphs (`convert_graph_to_tensor.py`), obtaining the stress & strain properties from the simulations (`obtain_stress_strain.py`), and assign the corresponding properties to graphs for generating the data (`convert_graph_to_tensor.py`) to train the GNN.

### Runing polycrystal plasticity using FEM

The polycrystal plasticity FEM simulations are run using mesh generated with [Neper](https://neper.info/) with [FEpX](https://fepx.info/index.html). To run the simulations, go to `fem_data/`, under Linux terminal (requires successful compilation of the two open source software):

```
$neper -T -n 10 -periodicity all -id 0
$neper -M n10-id0.tess
```

which generates the mesh and tessellation files for the 10-grain polyscrystal. Here, `id 0` identifies this specific polycrystal. Since there's the simulation control file `simulation.cfg`. You can run the simulation by:

```
$fepx
```

Please cite and refer to the website for detailed instructions for controlling & running the simulations.

To generate many different polycrystals and run the simulations correspondingly (on your cluster), you can either run the bash script (either use `sbatch`, `bash`, or else):

```
sbatch run_periodic.sh
```


### Converting FEM results to graphs

The `tetra10` mesh centroids (in *Neper* generated FEM meshes) were treated as nodes and their spatial connection were treated as edges. To first convert the mesh to `networkx` graphs (note that this process is slow; the figures for the graphs will be simultaneously generated):

```
python3 convert_mesh_to_graph.py
```

One then get all the calculated stress & strain from *FEpX* (FEM results are saved under `fem_data/sim_results`):

```
python3 obtain_stress_strain.py
```

One can then assign the properties to graphs and save as [Torch geometric tensors](https://pytorch-geometric.readthedocs.io/en/latest/).

```
python3 convert_graph_to_tensor.py
```

* **Example usage**: go to `fem_data/`, and run:

```
mkdir example_dat
cd example_dat/
```

and then run:

```
gdown https://drive.google.com/uc?id=15ZL4E4_30oa4LGZ1MkSq7HMrLIj34LnO
gdown https://drive.google.com/uc?id=1t66hAkXR5L2Wb2_SutXRcmcd4wHbOjEr
```

Now you will have two files, `reordered_all_graphs.pkl` and `reordered_stress_strain_data.pkl` (this will save your much time from the converting the graphs)

And you can directly run `python3 convert_graph_to_tensor.py`. Then you should be able to generate a file `all_graphs.pkl` (PyG tensor). In the same sense you can generate `all_graphs_unseen.pkl`

Note that you will need `networkx==3.1` with `python>=3.8` to do this calculation.

### Obtained graphs in the article

`all_graphs.pkl` contains all the FEM graphs. `graph_unseen_tensor.pkl` contains the unseen FEM graphs.

## Training

If you don't want to wait for hours & just want to check how the model is being trained (e.g., just test for 2 epochs). You can run (under `codes/`):

```
python3 -i train_model.py -ep 2
```

Note that the predictions may look pretty bad since the model is not fully trained. Your trained model can be found at `trained_model/TEST_MODEL_..pth`. To obtain decent predictions from the model, 1000 epochs is a good choice. 

The default subgraph training ratio is 0.5 (half of the full graph is extracted for training). To customize the training process, you may run:

```
python3 train_model.py -ep 2500 -tr 0.7
```

which trains the model on 70% of the full graph for 2500 epochs.

Here is a simple demonstration of the prediction results.

<img src="doc/gnn_fem_test.png" alt="demonstration of GNN" width="500">

## Predictions

To obtain the predictions of the pretrained model on the unseen dataset, you may run:

```
python3 obtain_gnn_pred.py
```


you can modify this file to obtain the predictions for the training & testing datasets (even though the predictions can already be obtained in the training scripts).

To be a developer and implement your own innovative ideas to this framework, you may modify `graph_utils.py`.


## Reproducing the figures in the paper

To generate the error plot, go to `fig/`, and run
```
python3 viz_error.py
```
you can reproduce the error distribution.

## Reference

Please cite our work:
```
@article{zhai2024gnn_polyplas,
  title={Stress Predictions in Polycrystal Plasticity using Graph Neural Networks with Subgraph Training},
  author={Zhai, Hanfeng},
  journal={arXiv preprint arXiv:2409.05169},
  year={2024}
}
```