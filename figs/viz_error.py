import os
import numpy as np
import matplotlib.pyplot as plt

def load_error_files(directory):
    data_list = []
    for root, _, files in os.walk(directory):
        for file in files:
            if file.endswith('.error'):
                file_path = os.path.join(root, file)
                data = np.loadtxt(file_path)
                data_list.append(data)
    return np.concatenate(data_list, axis=0)

# Load data from the directories
seen_data = load_error_files('gnn_pred_dat_seen')
unseen_data = load_error_files('gnn_pred_dat_unseen')

# Create figure 1 with 6 subfigures
fig1, axs1 = plt.subplots(2, 3, figsize=(10, 7))
fig1.suptitle('Errors comparison for stress elements')

for i in range(6):
    ax = axs1[i // 3, i % 3]
    ax.hist(seen_data[:, i], bins=256, alpha=0.5, label='Training & testing sets', color='blue', range=[0, 3500])
    ax.hist(unseen_data[:, i], bins=256, alpha=0.5, label='Unseen data', color='red', range=[0, 3500])
    ax.set_title(f'$\sigma_{i + 1}$')
    # ax.set_xscale('log')
    ax.set_yscale('log')
    ax.set_xlabel('MAE')
    ax.set_ylabel('Frequency')
    ax.legend()

fig1.tight_layout(rect=[0, 0.03, 1, 0.95])
fig1.savefig('error_stress_elem.pdf',dpi=300)
plt.close()
# Create figure 2 with 2 subfigures
fig2, axs2 = plt.subplots(1, 2, figsize=(7.5, 5))
fig2.suptitle('Errors generalization tests')

for i in range(6):
    axs2[0].hist(seen_data[:, i], bins=256, alpha=0.5, label=f'$\sigma_{i + 1}$')
    axs2[1].hist(unseen_data[:, i], bins=256, alpha=0.5, label=f'$\sigma_{i + 1}$')

for ax in axs2:
    ax.set_xlabel('MAE')
    ax.set_ylabel('Frequency')
    # ax.set_xscale('log')
    ax.set_yscale('log')
    ax.legend()

axs2[0].set_title('Training & testing sets')
axs2[1].set_title('Unseen data sets')

fig2.tight_layout(rect=[0, 0.03, 1, 0.95])
fig2.savefig('error_generalization.pdf',dpi=300)

print('Figures saved as figure1.png and figure2.png')
