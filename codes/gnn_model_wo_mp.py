import torch
from torch_geometric.nn import GCNConv, MessagePassing, GINConv, ChebConv, global_add_pool, global_mean_pool, PNAConv
from torch.nn import Sequential, Linear, BatchNorm1d, ReLU, MSELoss, Module, SiLU, Tanh, Sigmoid, Conv1d
from torch_scatter import scatter
import torch.nn.functional as F
import torch.nn as nn
import numpy as np
from torch.nn.init import constant_, eye_

model_hid_dim = 31
model_hid_layers = 1
node_fea_dim = 6
edge_fea_dim = 1
out_fea_dim = 6
device = torch.device("cuda") if torch.cuda.is_available() else torch.device("cpu")

class MPNNLayer(MessagePassing):
    def __init__(self, emb_dim=model_hid_dim, edge_dim=edge_fea_dim, node_dim=node_fea_dim, aggr_func='sum'):
        super().__init__()
        self.aggr_func = aggr_func
        self.emb_dim = emb_dim
        self.node_dim = node_dim
        self.edge_dim = edge_dim

        self.node_encode = Sequential(
            Linear(node_dim, emb_dim),
        )
        self.edge_encode = Sequential(
            Linear(edge_dim, emb_dim),  
        )
        self.messpass_decode = Sequential(
            Linear(emb_dim * 2, edge_dim), 
        )

    def forward(self, h, edge_index, edge_attr):
        '''double edge feature for message-passing'''
        h_emb = edge_attr#[:,0]
        edge_feat_dble = torch.cat([h_emb, h_emb], dim=0); #edge_feat_dble = torch.transpose(edge_feat_dble, 0, 1)
        h_i = h[edge_index[0,:]]; h_j = h[edge_index[1,:]]
        node_fea_dble = torch.cat([h_j, h_i], dim=0) 
        
        edge_feat_dble = edge_feat_dble.unsqueeze(0).T
        node_emb = torch.tanh(self.node_encode(node_fea_dble))
        edge_emb = torch.tanh(self.edge_encode(edge_feat_dble))

        index_e = torch.cat([torch.cat([edge_index[0,:], edge_index[1,:]], dim=0).view(1, -1)] * 2, dim=0)

        node_emb_sum = scatter(node_emb, index_e[0,:], dim=-2, reduce=self.aggr_func)
        edge_emb_sum = scatter(edge_emb, index_e[0,:], dim=-2, reduce=self.aggr_func)
        out = self.messpass_decode(torch.cat([node_emb_sum, edge_emb_sum], dim=1)); 
        return out

class GNN_FEM(Module):
    def __init__(self, num_layers=model_hid_layers, emb_dim=model_hid_dim, \
                 in_dim=node_fea_dim, edge_dim=edge_fea_dim, out_dim=out_fea_dim, aggr_func='sum'):
        super().__init__()
        # self.prelu_weight = torch.randn(model_hid_dim).to(device) # optional for prelu training
        self.ENCODE = Linear(in_dim, emb_dim, dtype=torch.float)
        self.EMB = Linear(emb_dim, emb_dim, dtype=torch.float)
        self.DECODE = Linear(emb_dim, out_dim, dtype=torch.float)
        self.convs = torch.nn.ModuleList()
        for layer in range(num_layers):
            self.convs.append(MPNNLayer(emb_dim, edge_dim, out_dim, aggr_func=aggr_func))

    def forward(self, data):
        link_sum = data.edge_attr#[:,0]
        for conv in self.convs:
            conv_output = conv(data.x, data.edge_index, data.edge_attr)
            min_size = min(data.x.size(0), conv_output.size(0))
            if data.x.size(0) > conv_output.size(0):
                data.x = data.x[:min_size,:]
            link_sum = conv(data.x, data.edge_index, data.edge_attr)
        encode = self.ENCODE(data.x)

        '''ReLU equation layer'''
        H = F.relu(self.EMB(encode)) * 1e3 # the method used in the paper

        decode = self.DECODE(H)
        return decode