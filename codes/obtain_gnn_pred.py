import random, sys, os

from graph_utils import generate_artificial_graphs, generate_presample_graphs,\
     train_model, test_model_accuracy, save_model, load_model, test_model_overall
import torch_geometric
import torch
import numpy as np
import torch.nn as nn
import torch.optim as optim
import pickle
import argparse
# from time import time # from tqdm import tqdm
from gnn_model import GNN_FEM

random_seed = 420
torch.manual_seed(random_seed)
torch.cuda.manual_seed_all(random_seed)
random.seed(random_seed); np.random.seed(random_seed)
torch.backends.cudnn.deterministic = True 

'''specify the parameters to be used'''
cur_dir = os.getcwd(); 
train_ratio = 0.8

device = torch.device("cuda") if torch.cuda.is_available() else torch.device("cpu")
if device.type=='cpu':
    raise Warning('The model presented in the paper is trained on GPU,\
        you may not be able to obtain predictions using {device}!')
elif device.type=='cuda':
    print(f'Your device is {device}')

print("Obtaining graphs from FEpX:")

with open(f"{cur_dir}/../data/graph_unseen_tensor.pkl", 'rb') as f:
    input_graphs_full = pickle.load(f)

output_dir = f"{cur_dir}/out_pred_unseen/"; os.makedirs(output_dir, exist_ok=True)
input_graphs = input_graphs_full

'''specify the model and required hyper-parameters'''
model = GNN_FEM(); model = model.to(device); print("GNN model:", model)
load_model(model, f"{cur_dir}/../trained_model/gnn_plasticity_relu.pth")

'''test both the training & testing graphs on the trained model'''
R2_list, bench_list, pred_list, test_R2_val = test_model_overall(input_graphs, len(input_graphs), model, output_dir)

os.makedirs('output/', exist_ok=True)
for i in range(30):
    pred_save = pred_list[i]
    bench_save = bench_list[i]
    abs_error = np.abs(pred_save - bench_save)
    fmt = ['%.7E'] * pred_save.shape[1]; print("shape:", pred_save.shape)
    np.savetxt(f"{cur_dir}/output/stress_{i}.pred", pred_save, delimiter=' ', fmt=fmt)
    np.savetxt(f"{cur_dir}/output/stress_{i}.error", abs_error, delimiter=' ', fmt=fmt)
    np.savetxt(f"{cur_dir}/output/stress_{i}.bench", bench_save, delimiter=' ', fmt=fmt)
print("Finish")