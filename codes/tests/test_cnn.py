import matplotlib.pyplot as plt
import torch
import torch.nn as nn
import torch.optim as optim
import numpy as np
import tqdm, random, argparse, glob
from sklearn.metrics import r2_score

parser = argparse.ArgumentParser(description="cnn for strain-stress mapping")
parser.add_argument("-hid", "--hid_dim", default=256, type=int)
args = parser.parse_args()

random_seed = 123
torch.manual_seed(random_seed)
torch.cuda.manual_seed_all(random_seed)
random.seed(random_seed)
np.random.seed(random_seed)
torch.backends.cudnn.deterministic = True

device = torch.device("cuda") if torch.cuda.is_available() else torch.device("cpu")

data_files = glob.glob("results/results_Rock*.npy")
input_list, output_list = [], []

for file in data_files:
    data = np.load(file, allow_pickle=True).item()
    epsilon11 = data["epsilon11"].reshape(100, 100) *1e7
    epsilon12 = data["epsilon12"].reshape(100, 100) *1e7
    epsilon22 = data["epsilon22"].reshape(100, 100) *1e7
    sigma11 = data["sigma11"].reshape(100, 100) #/ 1e6 * 100
    sigma12 = data["sigma12"].reshape(100, 100) #/ 1e6 * 100
    sigma22 = data["sigma22"].reshape(100, 100) #/ 1e6 * 100

    # depends on how we formulate the problem. Here it is σ -> ε
    output_tensor = torch.tensor(np.stack([epsilon11, epsilon12, epsilon22], axis=0), dtype=torch.float32).to(device)
    input_tensor = torch.tensor(np.stack([sigma11, sigma12, sigma22], axis=0), dtype=torch.float32).to(device)

    input_list.append(input_tensor)
    output_list.append(output_tensor)

class CNN_FEM(nn.Module):
    def __init__(self):
        super(CNN_FEM, self).__init__()
        self.conv1 = nn.Conv2d(3, int(args.hid_dim / 2), kernel_size=3, padding=1)
        self.conv2 = nn.Conv2d(int(args.hid_dim / 2), args.hid_dim, kernel_size=3, padding=1)
        self.conv3 = nn.Conv2d(args.hid_dim, 3, kernel_size=3, padding=1)
        self.relu = nn.ReLU()

    def forward(self, x):
        x = self.relu(self.conv1(x))
        x = self.relu(self.conv2(x))
        x = self.conv3(x)  # No activation for the last layer
        return x

model = CNN_FEM().to(device)
criterion = nn.MSELoss()
optimizer = optim.Adam(model.parameters(), lr=0.01)
scheduler = torch.optim.lr_scheduler.StepLR(optimizer, step_size=10000, gamma=0.5)

num_epochs = 1000; loss_list, r2_list, pred_list = [], [], []

for epoch in tqdm.tqdm(range(num_epochs)):
    epoch_losses, epoch_r2_scores = [], []
    step_pred, step_true = [], []
    for input_tensor, output_tensor in zip(input_list[:80], output_list[:80]):
        optimizer.zero_grad()  # Clear gradients
        predictions = model(input_tensor.unsqueeze(0))  # Add batch dimension
        loss = criterion(predictions, output_tensor.unsqueeze(0))  # Compute loss
        loss.backward()  # Backpropagation
        optimizer.step()  # Update weights

        epoch_losses.append(loss.item())

        step_pred.append(predictions); step_true.append(output_tensor); pred_list.append(predictions[0])
        r2 = r2_score(output_tensor.cpu().numpy().flatten(), predictions.cpu().detach().numpy().flatten())
        epoch_r2_scores.append(r2)

    avg_loss = np.mean(epoch_losses); avg_r2 = np.mean(epoch_r2_scores)
    loss_list.append(avg_loss); r2_list.append(avg_r2)
    step_pred = torch.stack(step_pred); step_true = torch.stack(step_true)

    if (epoch + 1) % 100 == 0:
        step_r2 = r2_score(step_pred.cpu().detach().numpy().flatten(), step_true.cpu().detach().numpy().flatten())
        print(f"Epoch [{epoch + 1}/{num_epochs}], Loss: {avg_loss:.6f}, R²: {avg_r2:.6f}")

test_id = 42; stress_id = 1
plt.figure(figsize=(9, 5))
plt.subplot(1, 2, 1); plt.title(f"FEM")
plt.imshow(output_list[test_id][stress_id].cpu().detach().numpy()/1e7, cmap='seismic', vmin=0.2, vmax=0.8); plt.colorbar()
plt.subplot(1, 2, 2); plt.title(f"CNN")
plt.imshow(step_pred[test_id][0][stress_id].cpu().detach().numpy()/1e7, cmap='seismic', vmin=0.2, vmax=0.8); plt.colorbar()#output_list[test_id][stress_id]
plt.tight_layout()
plt.savefig('strain_stress_predictions.png', dpi=300)

plt.figure(figsize=(5, 5))
plt.plot(loss_list, linewidth=2, alpha=0.75)
plt.xlabel('Epochs')
plt.ylabel('Loss')
plt.yscale('log')
plt.savefig('loss_curve.png', dpi=300)

'''model evaluation for all graphs'''
r2_eval = []
for input_tensor, output_tensor in zip(input_list, output_list):
    pred = model(input_tensor)
    R2 = r2_score(pred.cpu().flatten().detach().numpy(), output_tensor.cpu().flatten().detach().numpy())
    r2_eval.append(R2)
r2_eval = np.stack(r2_eval)
plt.figure(figsize=(5,5))
plt.hist(r2_eval[:80], bins=80, color='darkblue', alpha=0.75, label='Training sets')
plt.hist(r2_eval[80:], bins=20, color='darkred', alpha=0.75, label='Testing sets')
plt.xlim([0,1]); plt.legend(fontsize=12)
plt.xlabel(r'$\rm R^2$ using CNN', fontsize=15); plt.ylabel('Count', fontsize=15)
plt.savefig('cnn_r2', dpi=300)