# visualize model characterizations
python3 plot_model_char.py
python3 plot_model_char.py -file 'test_aggr_func'
python3 plot_model_char.py -file 'test_subgraph_ratio'
python3 plot_model_char.py -file 'test_training_size'