import matplotlib.pyplot as plt
from torch_geometric.data import Data
import numpy as np
import sys, os, tqdm, torch, random, pickle
sys.path.append(os.path.abspath(".."))
from gnn_model_2d import GNN_FEM
from sklearn.metrics import r2_score
import json; from networkx.readwrite import json_graph
from torch_geometric.utils import from_networkx
import networkx as ntx

random_seed = 123; torch.manual_seed(random_seed)
torch.cuda.manual_seed_all(random_seed)
random.seed(random_seed); np.random.seed(random_seed)
torch.backends.cudnn.deterministic = True
device = torch.device("cuda") if torch.cuda.is_available() else torch.device("cpu")
with open("graph_2DFEM.pkl", "rb") as f:
    graph_list = pickle.load(f)

data_list = []; print('processing graph data...') # i = 0
for G in tqdm.tqdm(graph_list):
    data = from_networkx(G)
    edge_index_sorted = torch.sort(data.edge_index, dim=0).values
    unique_edges = torch.unique(edge_index_sorted, dim=1)
    data.edge_index = unique_edges

    eps11 = torch.tensor(np.array([[node['epsilon11']] for _, node in G.nodes(data=True)]))
    eps12 = torch.tensor(np.array([[node['epsilon12']] for _, node in G.nodes(data=True)]))
    eps22 = torch.tensor(np.array([[node['epsilon22']] for _, node in G.nodes(data=True)]))

    sig11 = torch.tensor(np.array([[node['sigma11']] for _, node in G.nodes(data=True)]))
    sig12 = torch.tensor(np.array([[node['sigma12']] for _, node in G.nodes(data=True)]))
    sig22 = torch.tensor(np.array([[node['sigma22']] for _, node in G.nodes(data=True)]))

    data.x = torch.tensor(torch.cat([sig11, sig12, sig22], dim=1), dtype=torch.float) #eps11, eps12, 
    data.y = torch.tensor(torch.cat([eps11, eps12, eps22], dim=1), dtype=torch.float) #sig11, sig12, 
    src, dst = data.edge_index
    edge_attr = torch.norm(data.pos[src] - data.pos[dst], dim=1)
    data.edge_attr = edge_attr * 1000

    del data.sigma22; del data.epsilon22; del data.sigma12; del data.epsilon12; del data.sigma11; del data.epsilon11
    del data.pos; del data.material_marker

    data.x = (data.x / 1e6) * 1e2
    data.y = (data.y / 0.1) * 1e2
    data.edge_attr = data.edge_attr.float()
    data_list.append(data)

model = GNN_FEM(num_layers=3, emb_dim=256, in_dim=3, edge_dim=1, out_dim=3).to(device)
optimizer = torch.optim.Adam(model.parameters(), lr=0.01)
scheduler = torch.optim.lr_scheduler.StepLR(optimizer, step_size=10000, gamma=0.5)
criterion = torch.nn.MSELoss()

num_epochs = 1000
loss_list, r2_list = [], []; print('start training...')
for epoch in tqdm.tqdm(range(num_epochs)):
    epoch_loss, epoch_r2 = 0, 0
    dat_true, dat_pred = [], []
    for data in data_list[:80]:
        data = data.to(device)
        data.y.requires_grad_()
        model.train()
        optimizer.zero_grad()
        predictions = model(data)
        loss = criterion(predictions, data.y)
        loss.backward()
        torch.nn.utils.clip_grad_norm_(model.parameters(), max_norm=1.0)
        optimizer.step()

        R2 = r2_score(predictions.cpu().flatten().detach().numpy(), data.y.cpu().flatten().detach().numpy())
        epoch_loss += loss.item(); epoch_r2 += R2
        dat_true.append(data.y); dat_pred.append(predictions)

    loss_list.append(epoch_loss / len(data_list)); r2_list.append(np.round(epoch_r2,6))
    dat_true = torch.cat(dat_true); dat_pred = torch.cat(dat_pred); 

    if epoch % 100 == 0:
        R2_all = r2_score(dat_pred.cpu().flatten().detach().numpy(), dat_true.cpu().flatten().detach().numpy())
        print(f"Epoch {epoch} | Loss: {epoch_loss / len(data_list)}, R2: {R2_all}")#
        plt.figure(figsize=(5, 5))
        plt.semilogy(loss_list, linewidth=2, alpha=0.75)
        plt.xlabel('Epochs'); plt.ylabel('Loss'); plt.yscale('log'); plt.tight_layout()
        plt.savefig(f"loss_epoch_{epoch}.png", dpi=300); plt.close()

        plt.figure(figsize=(5, 5))
        plt.scatter(dat_true.cpu().detach().numpy(), dat_pred.cpu().detach().numpy(), s=10, color='darkblue', alpha=0.25, label=rf"$R^2$: {np.round(R2_all,2)}")
        plt.plot(np.linspace(-500,500,100), np.linspace(-500,500,100), '-.', linewidth=2, label=r"$y=x$", color='red')
        plt.legend(); plt.tight_layout(); plt.savefig('gnn_pred', dpi=300)
        plt.close('all')

plt.figure(figsize=(5, 5))
plt.semilogy(loss_list, linewidth=2, alpha=0.75)
plt.xlabel('Epochs');plt.ylabel('Loss')
plt.yscale('log'); plt.tight_layout()
plt.savefig('final_loss.png', dpi=300)

'''model evaluation for all graphs'''
r2_eval = []
for data in data_list:
    data = data.to(device)
    pred = model(data)
    R2 = r2_score(pred.cpu().flatten().detach().numpy(), data.y.cpu().flatten().detach().numpy())
    r2_eval.append(R2)
r2_eval = np.stack(r2_eval)
plt.figure(figsize=(5,5))
plt.hist(r2_eval[:80], bins=80, color='darkblue', alpha=0.75, label='Training sets')
plt.hist(r2_eval[80:], bins=20, color='darkred', alpha=0.75, label='Testing sets')
plt.xlim([0,1]); plt.legend(fontsize=12)
plt.xlabel(r'$\rm R^2$ using GNN', fontsize=15); plt.ylabel('Count', fontsize=15)
plt.savefig('gnn_r2', dpi=300)