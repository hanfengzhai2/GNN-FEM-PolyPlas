from fenics import *
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.tri import Triangulation

def coords_to_cell_indices(coords, mesh, Lx=10, Ly=10, nx=75, ny=75):
    cell_indices = []
    for x, y in coords:
        # Convert coordinates to normalized coordinates
        x_normalized = x / Lx * nx
        y_normalized = y / Ly * ny
        # Round to the nearest integer
        x_idx = int(round(x_normalized))
        y_idx = int(round(y_normalized))
        # Calculate the cell index
        cell_index = y_idx * (nx) + x_idx
        cell_indices.append(cell_index)
    return cell_indices

def Corner_point(x, on_boundary):
    tol = 1e-14
    return near(x[0], 0, tol) and near(x[1], 0, tol)

def markers_to_array(mesh, markers, nx, ny, Lx=10, Ly=10):
    # Initialize an array of zeros
    marker_array = np.zeros((ny + 1, nx + 1))
    for cell in cells(mesh):
        x, y = cell.midpoint().x(), cell.midpoint().y()
        x_idx = int(round(x / Lx * nx))
        y_idx = int(round(y / Ly * ny))
        marker_array[y_idx, x_idx] = markers[cell.index()]
    return marker_array

def visualize_marker(marker_array):
    plt.figure(figsize=(5,5))
    plt.imshow(marker_array, cmap='gray', origin='lower', interpolation='nearest')
    plt.colorbar(label='Marker Value')
    plt.title('Mesh Markers Visualization')
    plt.xlabel('x direction')
    plt.ylabel('y direction')
    plt.savefig('marker', dpi=300, transparent=True)
    
def plot_field(mesh, field, cmap='viridis', name='field', vmin=0, vmax=10, output_folder='.'):
    coordinates = mesh.coordinates()
    values = field.compute_vertex_values(mesh)
    print(values.shape)
    x, y = coordinates[:, 1], coordinates[:, 0]

    # triang = Triangulation(x, y)
    triang = Triangulation(x, y, mesh.cells())
    plt.figure()
    if vmin is not None and vmax is not None:
        levels = np.linspace(vmin, vmax, num=100)  # 100 levels between vmin and vmax
    else:
        levels = 100  # default number of levels

    plt.tricontourf(triang, values, levels=levels, cmap=cmap, vmin=vmin, vmax=vmax)
    plt.colorbar(label=f'{name}')
    plt.triplot(triang, 'k-', lw=0.5)
    plt.xlabel('x')
    plt.ylabel('y')
    plt.title(f'{name} field')
    plt.savefig(f'{output_folder}/{name}.png', dpi=300, transparent=True); plt.close()
    return values

def plot_imshow(grid, cmap='viridis', name='field', vmin=None, vmax=None):
    plt.figure()
    plt.imshow(grid, cmap=cmap, origin='lower', interpolation='nearest', vmin=vmin, vmax=vmax)
    plt.colorbar(label=f'{name}')
    plt.title(f'{name} field')
    plt.xlabel('x direction')
    plt.ylabel('y direction')
    plt.savefig(f'{name}_imshow.png', dpi=300, transparent=True)
    plt.show(); plt.close()

def plot_contourf(grid, cmap='viridis', name='field', vmin=None, vmax=None):
    plt.figure()
    x = np.arange(grid.shape[1])
    y = np.arange(grid.shape[0])
    X, Y = np.meshgrid(x, y)
    if vmin is not None and vmax is not None:
        levels = np.linspace(vmin, vmax, num=100)  # 100 levels between vmin and vmax
    else:
        levels = 100  # default number of levels

    plt.contourf(grid, cmap=cmap, levels=levels, vmin=vmin, vmax=vmax)
    plt.colorbar(label=f'{name}')
    plt.title(f'{name} field')
    plt.xlabel('x direction')
    plt.ylabel('y direction')
    plt.savefig(f'{name}_contourf.png', dpi=300, transparent=True)
    plt.show(); plt.close()
    
def plot_field_from_coord(coordinates, cells, values, cmap='viridis', name='field', vmin=0, vmax=10, output_folder='.'):
    # coordinates = mesh.coordinates()
    # values = field.compute_vertex_values(mesh)
    print(values.shape)
    x, y = coordinates[:, 1], coordinates[:, 0]

    # triang = Triangulation(x, y)
    triang = Triangulation(x, y, cells)
    plt.figure()
    if vmin is not None and vmax is not None:
        levels = np.linspace(vmin, vmax, num=100)  # 100 levels between vmin and vmax
    else:
        levels = 100  # default number of levels

    plt.tricontourf(triang, values, levels=levels, cmap=cmap, vmin=vmin, vmax=vmax)
    plt.colorbar(label=f'{name}')
    plt.triplot(triang, 'k-', lw=0.5)
    plt.xlabel('x')
    plt.ylabel('y')
    plt.title(f'{name} field')
    plt.savefig(f'{output_folder}/{name}.png', dpi=300, transparent=True); plt.close()
    return values