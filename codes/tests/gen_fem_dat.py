import numpy as np
from fenics import *
import networkx as ntx
import pickle
import os, re, random, glob
import matplotlib.pyplot as plt
from matplotlib.tri import Triangulation
from FEM_utils import coords_to_cell_indices, Corner_point, markers_to_array,\
    visualize_marker, plot_field, plot_imshow, plot_contourf
import tqdm
cur_dir = os.getcwd()

import argparse
parser = argparse.ArgumentParser(description="loading case")
parser.add_argument("-load", "--loading", default="normal", type=str, help="loading case")
parser.add_argument("-size", "--dom_size", default=32, type=int, help="size")
args = parser.parse_args()

os.makedirs('results', exist_ok=True); os.makedirs('results/fig', exist_ok=True)
save_folder = 'results'

rocks_2d_dir = 'data'
npz_files = glob.glob(os.path.join(rocks_2d_dir, '*.npz'))

np.random.seed(123)
nx, ny = 99, 99
Lx, Ly = 1.0, 1.0
slice_list = []
# ===============
from matplotlib.tri import Triangulation
def create_graph_from_mesh(mesh, epsilon11_G, sigma11_G, epsilon12_G, sigma12_G, epsilon22_G, sigma22_G, material_marker): #(mesh, sigma22_result, epsilon_yy_result, material_marker):
    G = ntx.Graph()
    coordinates = mesh.coordinates()
    cells = mesh.cells()
    sigma11 = sigma11_G.compute_vertex_values(mesh)
    epsilon11 = epsilon11_G.compute_vertex_values(mesh)
    sigma12 = sigma12_G.compute_vertex_values(mesh)
    epsilon12 = epsilon12_G.compute_vertex_values(mesh)
    sigma22 = sigma22_G.compute_vertex_values(mesh)
    epsilon22 = epsilon22_G.compute_vertex_values(mesh)
    material_marker_values = material_marker.vector().get_local()

    for cell_id, cell in tqdm.tqdm(enumerate(cells)):
        cell_coords = coordinates[cell]
        centroid = np.mean(cell_coords, axis=0)

        sigma11_mean = sigma11[cell].mean()
        epsilon11_mean = epsilon11[cell].mean()
        sigma12_mean = sigma12[cell].mean()
        epsilon12_mean = epsilon12[cell].mean()
        sigma22_mean = sigma22[cell].mean()
        epsilon22_mean = epsilon22[cell].mean()
        marker_value = material_marker_values[cell_id]  # Get material marker for the cell

        G.add_node(cell_id, pos=(centroid[0], centroid[1]),
                   sigma11=sigma11_mean, epsilon11=epsilon11_mean,
                   sigma12=sigma12_mean, epsilon12=epsilon12_mean,
                   sigma22=sigma22_mean, epsilon22=epsilon22_mean,
                   material_marker=marker_value)  # Add marker value

    for i, cell1 in tqdm.tqdm(enumerate(cells)):
        for j in range(i + 1, len(cells)):
            cell2 = cells[j]
            if len(set(cell1) & set(cell2)) == 2:  # Check if they share an edge
                G.add_edge(i, j)
    return G
# ===============
def epsilon(u):
    return sym(nabla_grad(u))

def sigma(u, material_marker):
    markers_at_cells = material_marker.vector().get_local()
    mu_array = np.full(markers_at_cells.shape, mu_vacuum)
    lame_array = np.full(markers_at_cells.shape, lame_const_vacuum)
    mu_array[markers_at_cells == 1] = mu_new_material
    lame_array[markers_at_cells == 1] = lame_const_new_material

    mu = Function(material_marker_space)
    mu.vector().set_local(mu_array)
    lame_const = Function(material_marker_space)
    lame_const.vector().set_local(lame_array)

    return lame_const * tr(epsilon(u)) * Identity(2) + 2 * mu * epsilon(u)

class Top_surface(SubDomain):
    def inside(self, x, on_boundary):
        tol = 1e-14
        return on_boundary and near(x[1], Ly, tol)

class Bottom_surface(SubDomain):
    def inside(self, x, on_boundary):
        tol = 1e-14
        return on_boundary and near(x[1], 0, tol)

class Left_surface(SubDomain):
    def inside(self, x, on_boundary):
        tol = 1e-14
        return on_boundary and near(x[0], 0, tol)

class Right_surface(SubDomain):
    def inside(self, x, on_boundary):
        tol = 1e-14
        return on_boundary and near(x[0], Lx, tol)

class PeriodicBoundary2D(SubDomain):
    def inside(self, x, on_boundary):
        return bool(
            (near(x[0], 0) or near(x[0], Lx)) and
            (near(x[1], 0) or near(x[1], Ly)) and
            on_boundary
        )

    def map(self, x, y):
        if near(x[0], Lx):
            y[0] = x[0] - Lx
        elif near(x[0], 0):
            y[0] = x[0] + Lx
        else:
            y[0] = x[0]
        
        if near(x[1], Ly):
            y[1] = x[1] - Ly
        elif near(x[1], 0):
            y[1] = x[1] + Ly
        else:
            y[1] = x[1]

graph_list = []
for npz_file in npz_files:
    print(f'data: {npz_file}')
    data = np.load(npz_file, allow_pickle=True)
    slice_2d = data[data.files[0]]
    padding_width = 0
    slice_2d = np.pad(slice_2d, pad_width=padding_width, mode='constant', constant_values=0)

    marker_array = slice_2d
    mu_vacuum = 1e-4 * (1e9)
    lame_const_vacuum = 9.3333e-04 * (1e9)
    mu_new_material = 45 * (1e9)
    lame_const_new_material = 6 * (1e9)

    mesh = RectangleMesh(Point(0, 0), Point(Lx, Ly), nx, ny)
    markers = MeshFunction("size_t", mesh, mesh.topology().dim(), 0)

    for cell in cells(mesh):
        x, y = cell.midpoint().x(), cell.midpoint().y()
        x_idx = int(round(x / Lx * nx))
        y_idx = int(round(y / Ly * ny))
        if marker_array[x_idx, y_idx] == 1:
            markers[cell.index()] = 1

    material_marker_space = FunctionSpace(mesh, 'DG', 0)
    material_marker_space._constrained_domain = PeriodicBoundary2D()

    material_marker = Function(material_marker_space)
    material_marker_values = np.zeros(mesh.num_cells(), dtype=np.float64)

    for cell in cells(mesh):
        x, y = cell.midpoint().x(), cell.midpoint().y()
        x_idx = int(np.round(x / Lx * (nx - 1)))
        y_idx = int(np.round(y / Ly * (ny - 1)))
        if marker_array[x_idx, y_idx] == 1:
            material_marker_values[cell.index()] = 1.0

    material_marker.vector().set_local(material_marker_values)
    material_marker.vector().apply("insert")

    '''define trial space for solutions'''
    V2 = VectorFunctionSpace(mesh, 'P', 2, constrained_domain=PeriodicBoundary2D())
    V = FunctionSpace(mesh, 'P', 1, constrained_domain=PeriodicBoundary2D())

    '''define boundaries'''
    top_surface, bottom_surface = Top_surface(), Bottom_surface()
    boundaries = MeshFunction("size_t", mesh, mesh.topology().dim()-1, 0)
    top_surface.mark(boundaries, 1); bottom_surface.mark(boundaries, 2)

    '''define loading'''
    if args.loading=='normal':
        dy_top = Expression('t * erate', t=0, erate=0.1, degree=2) # dy_bot = Expression('t * erate', t=0, erate=-0.1, degree=2)
        bc_top = DirichletBC(V2.sub(1), dy_top, boundaries, 1)
        bc_bottom = DirichletBC(V2.sub(1), Constant(0), boundaries, 2)
    elif args.loading=='shear':
        dx_top = Expression('t * erate', t=0, erate=0.1, degree=2) # dx_bot = Expression('t * erate', t=0, erate=-0.1, degree=2)
        bc_top = DirichletBC(V2.sub(0), dx_top, boundaries, 1)
        bc_bottom = DirichletBC(V2.sub(0), Constant(0), boundaries, 2)

    bcs = [bc_top, bc_bottom]

    u_n = interpolate(Constant((0, 0)), V2)
    u, v = TrialFunction(V2), TestFunction(V2)

    F = inner(sigma(u, material_marker), epsilon(v))*dx
    a, L = lhs(F), rhs(F)

    u = Function(V2); u_mag = sqrt(dot(u, u))
    s = sigma(u, material_marker) - (1./3)*tr(sigma(u, material_marker))*Identity(2)
    von_mises = sqrt(3./2*inner(s, s))

    '''time-stepping'''
    num_steps = 10
    dt = 0.1; t = 0
    for n in tqdm.tqdm(range(num_steps)):
        solve(a == L, u, bcs)

        t += dt; 
        if args.loading == 'normal':
            dy_top.t = t
        elif args.loading == 'shear':
            dx_top.t = t

        if n % 1 == 0 or n == num_steps-1:
            u_mag_result, von_mises_result = project(u_mag, V), project(von_mises, V)
            sigma11_result = project(sigma(u, material_marker)[0, 0], V)
            sigma22_result = project(sigma(u, material_marker)[1, 1], V)
            sigma12_result = project(sigma(u, material_marker)[0, 1], V)
            epsilon_xx_result = project(epsilon(u)[0, 0], V)
            epsilon_yy_result = project(epsilon(u)[1, 1], V)
            epsilon_xy_result = project(epsilon(u)[0, 1], V)

            nx_grid, ny_grid = nx + 1, ny + 1

        print('DATA SAVED')
    else:
        print('FINISHED')
    plot_field(mesh, epsilon_xx_result, cmap='viridis', name=f'epsilon11_{npz_file[-7:-4]}', \
        vmin=epsilon_xx_result.vector().get_local().min(), vmax=epsilon_xx_result.vector().get_local().max(), output_folder='results/fig')
    plot_field(mesh, epsilon_xy_result, cmap='viridis', name=f'epsilon12_{npz_file[-7:-4]}', \
        vmin=epsilon_xy_result.vector().get_local().min(), vmax=epsilon_xy_result.vector().get_local().max(), output_folder='results/fig')
    plot_field(mesh, epsilon_yy_result, cmap='viridis', name=f'epsilon22_{npz_file[-7:-4]}', \
        vmin=epsilon_yy_result.vector().get_local().min(), vmax=epsilon_yy_result.vector().get_local().max(), output_folder='results/fig')
    plot_field(mesh, sigma11_result, cmap='jet', name=f'sigma11_{npz_file[-7:-4]}', \
        vmin=sigma11_result.vector().get_local().min(), vmax=sigma11_result.vector().get_local().max(), output_folder='results/fig')
    plot_field(mesh, sigma12_result, cmap='jet', name=f'sigma12_{npz_file[-7:-4]}', \
        vmin=sigma12_result.vector().get_local().min(), vmax=sigma12_result.vector().get_local().max(), output_folder='results/fig')
    plot_field(mesh, sigma22_result, cmap='jet', name=f'sigma22_{npz_file[-7:-4]}', \
        vmin=sigma22_result.vector().get_local().min(), vmax=sigma22_result.vector().get_local().max(), output_folder='results/fig')
    
    '''some attempts to feed data to CNN'''
    epsilon22_G = epsilon_yy_result.compute_vertex_values(mesh)  # Input
    sigma22_G = sigma22_result.compute_vertex_values(mesh)  # Output
    epsilon11_G = epsilon_xx_result.compute_vertex_values(mesh)  # Input
    sigma11_G = sigma11_result.compute_vertex_values(mesh)  # Output
    epsilon12_G = epsilon_xy_result.compute_vertex_values(mesh)  # Input
    sigma12_G = sigma12_result.compute_vertex_values(mesh)  # Output
    
    from matplotlib.tri import Triangulation
    
    coord = mesh.coordinates()
    x, y = coord[:, 1], coord[:, 0]
    triang = Triangulation(x, y, mesh.cells())

    epsilon11 = (epsilon11_G - np.min(epsilon11_G)) / (np.max(epsilon11_G) - np.min(epsilon11_G))
    epsilon12 = (epsilon12_G - np.min(epsilon12_G)) / (np.max(epsilon12_G) - np.min(epsilon12_G))
    epsilon22 = (epsilon22_G - np.min(epsilon22_G)) / (np.max(epsilon22_G) - np.min(epsilon22_G))
    sigma11 = (sigma11_G - np.min(sigma11_G)) / (np.max(sigma11_G) - np.min(sigma11_G))
    sigma12 = (sigma12_G - np.min(sigma12_G)) / (np.max(sigma12_G) - np.min(sigma12_G))
    sigma22 = (sigma22_G - np.min(sigma22_G)) / (np.max(sigma22_G) - np.min(sigma22_G))

    plt.figure(figsize=(5,5))
    plt.imshow(epsilon22.reshape(100,100), cmap='jet', origin='lower', interpolation='nearest')
    plt.savefig('tmp1', dpi=300)

    plt.figure(figsize=(5,5))
    plt.imshow(sigma22.reshape(100,100), cmap='jet', origin='lower', interpolation='nearest')
    plt.savefig('tmp2', dpi=300)

    G = create_graph_from_mesh(mesh, epsilon_xx_result, sigma11_result, epsilon_xy_result, sigma12_result,\
        epsilon_yy_result, sigma22_result, material_marker)
    print(G.nodes[0])
    graph_list.append(G)

    results = {'epsilon11': epsilon11,
               'epsilon12': epsilon12,
               'epsilon22': epsilon22,
               'sigma11': sigma11,
               'sigma12': sigma12,
               'sigma22': sigma22}

    np.save(os.path.join(save_folder, f'results_Rock{npz_file[-7:-4]}.npy'), results)
    with open("test_G.pkl", "wb") as f:
        pickle.dump(graph_list, f); print('create graph!')

with open("graph_2DFEM.pkl", "wb") as f:
    pickle.dump(graph_list, f)