import re, argparse
import matplotlib.pyplot as plt
import numpy as np

parser = argparse.ArgumentParser(description="specify the parameters for model viz.")
parser.add_argument("-file", "--data_file", default='test_model_messpass', type=str)
args = parser.parse_args()

file_path = f"{args.data_file}.out"

with open(file_path, "r") as file:
    content = file.read()

train_scores_pattern = r"train R2 scores: \[(.*?)\]";   test_scores_pattern = r"test R2 scores: \[(.*?)\]"
overall_train_pattern = r"Overall train R2: ([0-9.]+)"; overall_test_pattern = r"Overall test R2: ([0-9.]+)"

train_scores = re.findall(train_scores_pattern, content); test_scores = re.findall(test_scores_pattern, content)
overall_train_r2 = re.findall(overall_train_pattern, content); overall_test_r2 = re.findall(overall_test_pattern, content)

train_set1, train_set2 = list(map(float, train_scores[0].split(", "))), list(map(float, train_scores[1].split(", ")))
test_set1, test_set2 = list(map(float, test_scores[0].split(", "))), list(map(float, test_scores[1].split(", ")))
overall_train_r2_set1, overall_train_r2_set2 = float(overall_train_r2[0]), float(overall_train_r2[1])
overall_test_r2_set1, overall_test_r2_set2 = float(overall_test_r2[0]), float(overall_test_r2[1])

if args.data_file=='test_model_messpass':
    key1 = '(w/ message-passing)'
    key2 = '(w/o message-passing)'
    key1_2, key2_2 = '(w/ MP)', '(w/o MP)'
elif args.data_file=='test_subgraph_ratio':
    key1 = r'subgraph $25\%$'
    key2 = r'subgraph $50\%$'
    key1_2, key2_2 = r'$25\%$', r'$50\%$'

if args.data_file=='test_model_messpass' or args.data_file=='test_subgraph_ratio':
    plt.figure(figsize=(5, 5))
    plt.hist(train_set1, bins=30, alpha=0.5, label=f"Training set {key1}", color="darkblue")
    plt.hist(train_set2, bins=30, alpha=0.5, label=f"Training set {key2}", color="darkred")
    plt.hist(test_set1, bins=15, alpha=0.5, label=f"Testing set {key1}", color="cyan")
    plt.hist(test_set2, bins=15, alpha=0.5, label=f"Testing set {key2}", color="orange")

    plt.axvline(overall_train_r2_set1, color="darkblue", linestyle="-.", linewidth=2, label=rf"Overall train $R^2$ {key1_2}: {overall_train_r2_set1}")
    plt.axvline(overall_train_r2_set2, color="darkred", linestyle="-.", linewidth=2, label=f"Overall train $R^2$ {key2_2}: {overall_train_r2_set2}")
    plt.axvline(overall_test_r2_set1, color="cyan", linestyle="--", linewidth=1, label=f"Overall test $R^2$ {key1_2}: {overall_test_r2_set1}")
    plt.axvline(overall_test_r2_set2, color="orange", linestyle="--", linewidth=1, label=f"Overall test $R^2$ {key2_2}: {overall_test_r2_set2}")
    
    plt.xlabel(r"$\rm R^2$", fontsize=15)
    plt.ylabel("Counts", fontsize=15)
    plt.legend(fontsize=10)
    plt.title('Full graph training', fontsize=15)
    plt.grid(True)
    plt.savefig(f'model_char_{args.data_file}', dpi=300)

'''==============================================================='''
if args.data_file=='test_aggr_func': #'test_training_size' or 
    train_set3, train_set4, train_set5 = list(map(float, train_scores[2].split(", "))), list(map(float, train_scores[3].split(", "))), list(map(float, train_scores[4].split(", ")))
    test_set3, test_set4, test_set5 = list(map(float, test_scores[2].split(", "))), list(map(float, test_scores[3].split(", "))), list(map(float, test_scores[4].split(", ")))

    overall_train_r2_set3, overall_train_r2_set4, overall_train_r2_set5 = float(overall_train_r2[2]), float(overall_train_r2[3]), float(overall_train_r2[4])
    overall_test_r2_set3, overall_test_r2_set4, overall_test_r2_set5 = float(overall_test_r2[2]), float(overall_test_r2[3]), float(overall_test_r2[4])
    
    train_sets = [train_set1, train_set2, train_set3, train_set4, train_set5]
    test_sets = [test_set1, test_set2, test_set3, test_set4, test_set5]
    overall_train_r2 = [overall_train_r2_set1, overall_train_r2_set2, overall_train_r2_set3, overall_train_r2_set4, overall_train_r2_set5]
    overall_test_r2 = [overall_test_r2_set1, overall_test_r2_set2, overall_test_r2_set3, overall_test_r2_set4, overall_test_r2_set5]
    keys = ['sum', 'add', 'mean', 'min', 'max']
    colors_train = ["darkblue", "darkred", "darkgreen", 'darkorange', 'darkcyan']
    colors_test = ["blue", "red", "green", 'orange', 'cyan']

    plt.figure(figsize=(7.5, 5))
    for i, (train, test, train_r2, test_r2, key, c_train, c_test) in enumerate(zip(
        train_sets, test_sets, overall_train_r2, overall_test_r2, keys, colors_train, colors_test)):
        plt.hist(train, bins=30, alpha=0.5, label=f"Training set {key}", color=c_train)
        plt.hist(test, bins=15, alpha=0.5, label=f"Testing set {key}", color=c_test)

        plt.axvline(train_r2, color=c_train, linestyle="-.", linewidth=2)#, label=f"Overall train $R^2$ {key}: {train_r2}")
        plt.axvline(test_r2, color=c_test, linestyle="--", linewidth=1)#, label=f"Overall test $R^2$ {key}: {test_r2}")

    plt.xlabel(r"$\rm R^2$", fontsize=15)
    plt.ylabel("Counts", fontsize=15)
    plt.legend(fontsize=10, loc='center left', bbox_to_anchor=(1.05, 0.5), borderaxespad=0)
    plt.title('Full graph training comparison', fontsize=15)
    plt.grid(True)
    plt.tight_layout(rect=[0, 0, 0.9, 1])
    plt.savefig(f'model_char_{args.data_file}', dpi=300)
    plt.show()

'''==============================================================='''
if args.data_file=='test_training_size':
    train_set3 = list(map(float, train_scores[2].split(", "))); train_set4 = list(map(float, train_scores[3].split(", "))); train_set5 = list(map(float, train_scores[4].split(", ")))
    train_set6 = list(map(float, train_scores[5].split(", "))); train_set7 = list(map(float, train_scores[6].split(", "))); train_set8 = list(map(float, train_scores[7].split(", ")))
    test_set3 = list(map(float, test_scores[2].split(", "))); test_set4 = list(map(float, test_scores[3].split(", "))); test_set5 = list(map(float, test_scores[4].split(", ")))
    test_set6 = list(map(float, test_scores[5].split(", "))); test_set7 = list(map(float, test_scores[6].split(", "))); test_set8 = list(map(float, test_scores[7].split(", ")))
    overall_train_r2_set3 = float(overall_train_r2[2]); overall_train_r2_set4 = float(overall_train_r2[3]); overall_train_r2_set5 = float(overall_train_r2[4])
    overall_train_r2_set6 = float(overall_train_r2[5]); overall_train_r2_set7 = float(overall_train_r2[6]); overall_train_r2_set8 = float(overall_train_r2[7])
    overall_test_r2_set3 = float(overall_test_r2[2]); overall_test_r2_set4 = float(overall_test_r2[3]); overall_test_r2_set5 = float(overall_test_r2[4])
    overall_test_r2_set6 = float(overall_test_r2[5]); overall_test_r2_set7 = float(overall_test_r2[6]); overall_test_r2_set8 = float(overall_test_r2[7])
    
    train_sets = [train_set1, train_set2, train_set3, train_set4, train_set5, train_set6, train_set7, train_set8]
    test_sets = [test_set1, test_set2, test_set3, test_set4, test_set5, test_set6, test_set7, test_set8]
    overall_train_r2 = [overall_train_r2_set1, overall_train_r2_set2, overall_train_r2_set3, overall_train_r2_set4, overall_train_r2_set5, overall_train_r2_set6, overall_train_r2_set7, overall_train_r2_set8]
    overall_test_r2 = [overall_test_r2_set1, overall_test_r2_set2, overall_test_r2_set3, overall_test_r2_set4, overall_test_r2_set5, overall_test_r2_set6, overall_test_r2_set7, overall_test_r2_set8]
    keys = [r'$20\%$', r'$30\%$', r'$40\%$', r'$50\%$', r'$60\%$', r'$70\%$', r'$80\%$']
    colors_train = ["darkblue", "darkred", "darkgreen", 'darkorange', 'darkcyan', 'purple', 'deepskyblue', 'darksalmon']
    colors_test = ["blue", "red", "green", 'orange', 'cyan', 'pink', 'skyblue', 'salmon']

    plt.figure(figsize=(7.5, 5))
    for i, (train, test, train_r2, test_r2, key, c_train, c_test) in enumerate(zip(
        train_sets, test_sets, overall_train_r2, overall_test_r2, keys, colors_train, colors_test)):
        plt.hist(train, bins=35, alpha=0.5, label=f"Training size {key}", color=c_train)
        plt.hist(test, bins=35, alpha=0.5, label=f"Corr. testing set", color=c_test) #

        plt.axvline(train_r2, color=c_train, linestyle="-.", linewidth=2)#, label=f"Overall train $R^2$ {key}: {train_r2}")
        plt.axvline(test_r2, color=c_test, linestyle="--", linewidth=1)#, label=f"Overall test $R^2$ {key}: {test_r2}")
        plt.text(train_r2, plt.gca().get_ylim()[1] * (0.35 + i*0.05),  # Adjust 0.9 to position vertically
             f"{train_r2:.3f}", color=c_train, fontsize=10, ha='right', va='bottom')
        plt.text(test_r2, plt.gca().get_ylim()[1] * (0.5 + i*0.05),  # Adjust 0.9 to position vertically
             f"{test_r2:.3f}", color=c_test, fontsize=10, ha='left', va='bottom')


    plt.xlabel(r"$\rm R^2$", fontsize=15)
    plt.ylabel("Counts", fontsize=15)
    plt.legend(fontsize=10, loc='center left', bbox_to_anchor=(1.05, 0.5), borderaxespad=0)
    plt.title('Full graph training comparison', fontsize=15)
    plt.grid(True)
    plt.tight_layout(rect=[0, 0, 0.9, 1])
    plt.savefig(f'model_char_{args.data_file}', dpi=300)
    plt.show()