import numpy as np
from S2M.io import inpwriter,reader,writer
import matplotlib.pylab as plt
import S2M.vox2tet as v2t
import os, tqdm

save_npz = False
test_viz = True; slice_id = 0

def generate_inclusion_spherical(radius, center, nbvoxels, filepath):
    """
    generate 3D binary numpy array of a spherical inclusion
    in unit cubic box matrix
    Args :
        - radius
        - center : array of 3 float
        - nbvoxels : number of voxels along each axes
        - filepath : path and name of numpy save file (.npy)
    Return :
        - img
        - f : inclusion volume fraction
    """
    xc = center[0]; yc = center[1]; zc = center[2]
    x = np.linspace(-0.5, 0.5, nbvoxels)
    X, Y, Z = np.meshgrid(x, x, x)

    t = (X - xc)**2 + (Y - yc)**2 + (Z - zc)**2 < radius**2

    img_ind = np.argwhere(t==True)
    img = np.zeros((nbvoxels, nbvoxels, nbvoxels), dtype=int)
    img[img_ind[:, 0], img_ind[:,1], img_ind[:,2]] = 1
    np.save(filepath + '.npy', img)

    f = compute_phase_volume_fraction(img, 1)
    return img, f, t


def compute_phase_volume_fraction(img, label):
    '''
    compute volume fraction of a phase in 3D image
    Args :
        - img
        - lab
    Return :
        - f : inclusion volume fraction
    '''
    N = img.size
    nlab = np.sum(img==label)
    f = 100. * (nlab) / N
    return f


if __name__ == '__main__':
    directory_path = os.getcwd() + os.path.sep

    R = 0.08 # radius of sphere
    nb_voxels = 100
    nb_sphere_max = 200
    f_target = 0.30
    filename = 'microstructure'
    os.makedirs('data', exist_ok=True)

    voxels = np.zeros((nb_voxels, nb_voxels, nb_voxels), dtype=int)
    micro_bin = (voxels == 1)
    f = 0.
    for i in tqdm.tqdm(range(nb_sphere_max)):
        center = np.random.uniform(-0.5,0.5,3)
        img, fp, img_bin = generate_inclusion_spherical(R, center, nb_voxels,
                                                    directory_path + 'data/' + filename)
        micro_bin = micro_bin + img_bin
        f = 1. * np.sum(micro_bin) / micro_bin.size
        print("-> volume fraction target : {0} ; volume fraction reached : {1} ; nb spheres inserted : {2}".format(f_target,f,i))
        if f >= f_target:
            break

    micro_bin_int = micro_bin.astype(np.int8)
    if save_npz:
        for i in tqdm.tqdm(range(micro_bin_int.shape[0])):
            slice_2d = micro_bin_int[i, :, :]
            output_path = os.path.join('data/', f"slice_{i:03d}.npz")
            np.savez_compressed(output_path, slice=slice_2d)

    if test_viz:
        # micro_ind = np.argwhere(micro_bin==True)
        # fig = plt.figure(figsize=(5, 5)) # axe = fig.add_subplot(111)
        # plt.imshow(micro_bin[:,:,slice_id].T, cmap=plt.get_cmap('binary')) #im = 
        # plt.savefig(directory_path + filename + f'{slice_id}' + '.png', dpi=900, transparent=False)

        '''for testing purposes'''
        microstructure = np.load('data/slice_000.npz')
        results = np.load('results/results_Rock000.npy', allow_pickle=True)
        results_dict = results.item()

        epsilon22, sigma22 = results_dict['epsilon22'], results_dict['sigma22']
        fig, axes = plt.subplots(1, 3, figsize=(12, 3.5))
        
        axes[0].imshow(microstructure['slice'].T, cmap=plt.get_cmap('binary'))
        axes[0].set_title('Microstructure', fontsize=15)# axes[0].axis('off')

        im1 = axes[2].imshow(epsilon22.reshape(100,100), cmap='seismic', aspect='auto')
        axes[2].set_title(r'$\epsilon_{22}$', fontsize=15)
        fig.colorbar(im1, ax=axes[2])

        im2 = axes[1].imshow(sigma22.reshape(100,100), cmap='seismic', aspect='auto')
        axes[1].set_title(r'$\sigma_{22}$', fontsize=15)
        fig.colorbar(im2, ax=axes[1])

        plt.tight_layout()
        plt.savefig(directory_path + filename + f'{slice_id}' + '.png', dpi=900, transparent=False)