import random, sys, os

from graph_utils import generate_artificial_graphs, generate_presample_graphs,\
     train_model, test_model_accuracy, save_model, load_model, test_model_overall
import torch_geometric
import torch
import numpy as np
import torch.nn as nn
import torch.optim as optim
import pickle
import argparse
# from time import time # from tqdm import tqdm
from datetime import date
from gnn_model import GNN_FEM
parser = argparse.ArgumentParser(description="specify the parameters for the test case")
parser.add_argument("-r", "--randomseed", default=420, type=int, help="specify the random seed number")
parser.add_argument("-ng", "--numgraphs", default=100, type=int, help="specify the total number of graphs")
parser.add_argument("-ep", "--epochs", default=1000, type=int, help="specify the total number of epochs")
parser.add_argument("-tr", "--train_ratio", default=0.5, type=float, help="specify the train ratio of graphs")
parser.add_argument("-no_train", "--no_train", action='store_true')
parser.add_argument("-aggr", "--aggr_func", default='sum', type=str)
args = parser.parse_args()

'''for reproducibility purposes'''
random_seed = args.randomseed
torch.manual_seed(random_seed)
torch.cuda.manual_seed_all(random_seed)
random.seed(random_seed); np.random.seed(random_seed)
torch.backends.cudnn.deterministic = True 

'''specify the parameters to be used'''
cur_dir = os.getcwd()
train_ratio = args.train_ratio
device = torch.device("cuda") if torch.cuda.is_available() else torch.device("cpu")
if device.type=='cpu':
    raise Warning('GPU is recommended for training GNN, your current device is {device}!')
elif device.type=='cuda':
    print(f'Your device is {device}')

print("Obtaining graphs from FEpX:")
with open(f"{cur_dir}/../data/all_graphs.pkl", 'rb') as f:
    input_graphs_full = pickle.load(f)[:90]

random.shuffle(input_graphs_full)
print(f'    ->    Your Training & Testing Sets size is {len(input_graphs_full)}')
output_dir = f"{cur_dir}/out_gnn_train_ep{args.epochs}/"; os.makedirs(output_dir, exist_ok=True)
input_graphs = input_graphs_full
train_end_index = int(len(input_graphs) * args.train_ratio) 
random.shuffle(input_graphs)
if not args.no_train:
    '''generate presampled torch geometric tensor'''; print(f'Presampling graphs for subgraph training for {args.epochs} epochs:')
    batch_overall, subG_index, fullG_index, batch_active_ind = generate_presample_graphs(args.epochs, input_graphs[:72], train_ratio) # 72 is the number of training graphs
num_train_graphs = 72

'''specify the model and required hyper-parameters'''
model = GNN_FEM(aggr_func=args.aggr_func); model = model.to(device); print("GNN model:", model)

optimizer = optim.Adam(model.parameters(), lr=0.001)
criterion = nn.MSELoss()
if not args.no_train:
    '''training the model'''; print(f'Your are training on {num_train_graphs} graphs:')
    model, _ = train_model(batch_overall, batch_active_ind, output_dir,\
                            num_train_graphs, args.epochs, optimizer, criterion, model)
if args.no_train:
    load_model(model, "../trained_model/gnn_plasticity_relu.pth") # optional: load pretrained model

'''saving the model'''
today = date.today(); os.makedirs('trained_model', exist_ok=True)
save_model(model, f"../trained_model/TEST_MODEL_ep{args.epochs}_{today}.pth") # `gnn_plasticity_relu.pth` -> the one presented in the paper
'''test both the training & testing graphs on the trained model'''
R2_list, bench_list, pred_list, test_R2_val = test_model_accuracy(input_graphs, len(input_graphs), 1, model, output_dir)
print("Finish training & testing")