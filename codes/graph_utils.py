# from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import os, sys
import numpy as np
import torch
import torch_geometric
from torch_geometric.utils import subgraph
from torch_geometric.data import Data, Batch
# from torch_geometric.utils import subgraph
import random
import pickle
from tqdm import tqdm
from sklearn.metrics import r2_score
from scipy.stats.stats import pearsonr 
import networkx as nx
# sys.path.append("../../DDD_avg_test/")

random_seed = 420
torch.manual_seed(random_seed)
torch.cuda.manual_seed_all(random_seed)
random.seed(random_seed)
np.random.seed(random_seed)
torch.backends.cudnn.deterministic = True 
# * data transform modified

SIZE_ARMS_DATA = 10
device = torch.device("cuda") if torch.cuda.is_available() else torch.device("cpu")


def reorder_edge_index(edge_index):
    flat_edge_index = edge_index.flatten()
    unique_values, unique_indices = torch.unique(flat_edge_index, return_inverse=True)
    mapping = {old_idx.item(): new_idx.item() for old_idx, new_idx in zip(unique_values, torch.arange(unique_values.size(0)))}
    reordered_flat_edge_index = torch.tensor([mapping[idx.item()] for idx in flat_edge_index], dtype=torch.long)
    reordered_edge_index = reordered_flat_edge_index.view(edge_index.size())
    return reordered_edge_index

def save_model(model, filepath):
    torch.save(model.state_dict(), filepath)
    print(f"Model saved at {filepath}")

def load_model(model, filepath):
    model.load_state_dict(torch.load(filepath))
    model.eval()
    print(f"Model loaded from {filepath}")

def calculate_link_length(node_positions, edge_indices):
    lengths = []
    for edge_index in edge_indices.t():
        pos1 = node_positions[edge_index[0]]
        pos2 = node_positions[edge_index[1]]
        length = torch.linalg.norm(pos2 - pos1, dim=-1, keepdim=True)
        lengths.append(length)
    return torch.cat(lengths, dim=0)

def get_connected_subgraph(graph, start_node):
    visited = set()
    subgraph_nodes = []

    def dfs(node):
        if node not in visited:
            visited.add(node)
            subgraph_nodes.append(node)
            for neighbor in graph.neighbors(node):
                dfs(neighbor)

    dfs(start_node)
    return subgraph_nodes

def graph_dim_transform(subgraph_data, model):
    subcheck = []; subcheck.append(subgraph_data)
    subG = Batch.from_data_list(subcheck).to(device)
    subG.x = subG.x.to(torch.float)
    subG.edge_index = subG.edge_index.to(torch.long)
    subG.edge_attr = subG.edge_attr.to(torch.float)
    check_y = model(subG)
    trimed_y = check_y.size(0)
    subgraph_data.x = subgraph_data.x[:trimed_y,:]
    subgraph_data.y = subgraph_data.y[:trimed_y,:]
    return subgraph_data

def batch_tranform(batch):
    batch.x = batch.x.to(torch.float)
    batch.edge_index = batch.edge_index.to(torch.long)
    batch.edge_attr = batch.edge_attr.to(torch.float)
    return batch

def calc_node_avg_num_order(G, x, inp_dim): # Approach 1
    num_nodes = len(G)
    y = torch.zeros(num_nodes, inp_dim)
    for i in range(num_nodes):
        neighbors = list(G.neighbors(i))
        if neighbors:
            neighbor_features = torch.stack([x[n] for n in neighbors])
            y[i] = torch.mean(neighbor_features, dim=0)
        else:
            y[i] = x[i]
    return y

def calculate_node_avg(graph):
    force_sum = {}; link_sum = {}
    for node in graph.nodes:
        neighbors = list(graph.neighbors(node))

        eps = 1e-6; nodes_to_remove = []; edge_sum = np.zeros(3)+eps
        if neighbors:
            node_sum = np.zeros(3);   # Initialize a vector to store the sum of 'F' attribute elements
            num_neighbors_with_attribute = 0; 
                # node_sum += graph.nodes[node]['F']
            for neighbor in neighbors:
                if 'F' in graph.nodes[neighbor]:
                    node_sum += graph.nodes[neighbor]['F']
                    num_neighbors_with_attribute += 1

        for edge in graph.edges(node, data=True):
            if 'link_len' in edge[2]:
                edge_sum += edge[2]['link_len']
            
            if num_neighbors_with_attribute > 0:
                force_sum[node] = [x for x in node_sum] #/ num_neighbors_with_attribute
                # neigh_sum[node] = [x / num_neighbors_with_attribute for x in node_sum]
                link_sum[node] = [x for x in edge_sum]
                graph.add_node(node, F_sum = force_sum[node], link_sum = link_sum[node])

    for node in graph.nodes:
        if 'F_sum' not in graph.nodes[node]:
            nodes_to_remove.append(node)
        elif 'link_sum' not in graph.nodes[node]:
            nodes_to_remove.append(node)
    for node in nodes_to_remove:
        graph.remove_node(node)
    return graph


def assign_graph_prop(G):
    # num_lab = 0; num_label_list = []; rand_label_list = []
    # if option == "ring":
    # for node in G.nodes:
    #     random_label = random.randint(1, 100)
    #     if (0, random_label) in G.nodes:
    #         random_label += random.randint(-100, 1)
    #         random_label = np.abs(random_label)
    for node in G.nodes:
        random_label = random.randint(1, 100)
        if (0, random_label) in G.nodes:
            random_label += random.randint(-100, 1)
            random_label = abs(random_label)
        G = nx.relabel_nodes(G, {node: (0, random_label)})
            
    for node in G.nodes:
        force_x = random.uniform(-500, 500)
        force_y = random.uniform(-500, 500)
        force_z = random.uniform(-500, 500)
        G.nodes[node]['F'] = [force_x, force_y, force_z]
        G.add_node((0, random_label), F = [force_x, force_y, force_z])

    # existing_nodes = list(G.nodes())
    for edge in G.edges:
        # node1, node2 = random.sample(existing_nodes, 2)
        dir_x = random.uniform(0, 1)
        dir_y = random.uniform(0, 1)
        dir_z = random.uniform(0, 1)
        norm_dir = np.sqrt(dir_x**2+dir_y**2+dir_z**2)
        link_len = random.uniform(5, 20)

        G.edges[edge]['dir_vec'] = [dir_x/norm_dir, dir_y/norm_dir, dir_z/norm_dir]
        G.edges[edge]['link_len'] = link_len
        # G.add_edge(node1, node2, dir_vec=[dir_x, dir_y, dir_z], link_len=link_len)  # Add 'burg' attribute to edge

    G = calculate_node_avg(G); G = plane_projection(G); 
    return G


def gen_scratch_subgraph(Gph, train_ratio):#, model):
    # device = "cpu"
    node_features_in = Gph.x #torch.tensor([[node['force_x'], node['force_y'], node['force_z']] for node in Gph.nodes.values()])
    node_features_out = Gph.y

    data_input = Data(#MyData
        x=node_features_in.float(),
        edge_index=Gph.edge_index,#edge_index_in_reordered,
        edge_attr=Gph.edge_attr,#edge_features,
        y=node_features_out.float()
    )
    data = data_input.to(device)
    num_nodes = Gph.x.size(0)
    # subgraph_nodes = list(np.random.choice(num_nodes, int(train_ratio*num_nodes), replace=False))
    subgraph_size = int(train_ratio * num_nodes)
    subgraph_nodes = np.random.choice(num_nodes, subgraph_size, replace=False)

    data = data.to(device)

    subgraph_nodes_tensor = torch.tensor(subgraph_nodes, dtype=torch.long, device=device)
    subgraph_data = data.subgraph(subgraph_nodes_tensor)
    subgraph_index = torch_geometric.utils.subgraph(subset=subgraph_nodes_tensor, edge_index=data.edge_index)
    # print("check shape",subgraph_data.edge_index.shape, subgraph_index[0].shape)
    # subgraph_nx = torch_geometric.utils.to_networkx(subgraph_data)
    # is_weakly_connected = nx.is_weakly_connected(subgraph_nx)


    # if not is_weakly_connected:
    #     weakly_connected_components = nx.weakly_connected_components(subgraph_nx)
    #     largest_weakly_connected_component = max(weakly_connected_components, key=len)
        
    #     subgraph_nodes_tensor = torch.tensor(list(largest_weakly_connected_component), dtype=torch.long, device=device)
    #     subgraph_data = data.subgraph(subgraph_nodes_tensor)

    subgraph_data = subgraph_data.to(device)
    
    return subgraph_data, subgraph_index[0]


def prefactor_node(original_index, filtered_index):
    rows_to_keep = []; #basis = torch.zeros_like(original_index); #print(basis)
    max_index = int(torch.max(original_index[1]) + 1)
    for i in range(max_index):
        count_original1 = torch.sum(original_index[0] == i).item()
        count_filtered1 = torch.sum(filtered_index[0] == i).item()
        count_original2 = torch.sum(original_index[1] == i).item()
        count_filtered2 = torch.sum(filtered_index[1] == i).item()

        if count_original1 == count_filtered1 and count_original2 == count_filtered2:
            rows_to_keep.append(i)
    # filtered_matrix = np.zeros_like(original_matrix)  # Initialize filtered matrix with zeros
    # selected_rows0 = np.where(np.isin(original_index[0], rows_to_keep))[0]; 
    # selected_rows1 = np.where(np.isin(original_index[1], rows_to_keep))[0]; 
    # selected_rows = np.concatenate((selected_rows0, selected_rows1))
    # filtered_matrix[rows_to_keep] = original_matrix[rows_to_keep]  # Set rows to keep from original matrix
    # non_zero_rows = filtered_matrix.any(axis=1)
    return rows_to_keep#torch.Tensor(filtered_matrix)


def generate_presample_graphs(epoch_num, input_graphs, train_ratio):
    
    batch_overall = []; batch_index = []; batch_full_index = []; batch_active_ind = []
    for epoch in tqdm(range(epoch_num)):
        batch_data = []; sub_index = []; full_index = []; active_ind = []
        for Gph in input_graphs:
            subgraph_data, subgraph_index = gen_scratch_subgraph(Gph, train_ratio)#, model)#gen_subgraph(datafile_dir, DIR_G_in, DIR_G_out, graph_file, train_ratio, model)
            # print(subgraph_data)
            if subgraph_data.x.shape == subgraph_data.y.shape:
                batch_data.append(subgraph_data)
                sub_index.append(subgraph_index.to(device))
                full_index.append(Gph.edge_index.to(device))
                active_index = prefactor_node(torch.Tensor(Gph.edge_index.cpu().detach().numpy()),\
                                              torch.Tensor(subgraph_index.cpu().detach().numpy()))
                
                active_index_2 = prefactor_subgraph_index(subgraph_index, subgraph_data, active_index)
                active_ind.append(active_index_2)

        # batch = Batch.from_data_list(batch_data).to(device); # this line is not rigorous
        # batch = batch_tranform(batch)
        # print("sampling:", epoch)
        batch_overall.append(batch_data)
        batch_index.append(sub_index)
        batch_full_index.append(full_index)
        batch_active_ind.append(active_ind)
        
    return batch_overall, batch_index, batch_full_index, batch_active_ind


def generate_artificial_graphs(num_graphs, num_nodes, num_edges, rand_flag="random", test_type="mobility"):
    input_graphs = []; graphs = []
    for _ in range(num_graphs):
        if rand_flag == "random":
            if test_type in ["node_sum", "edge_sum", "mobility", "mobility_proj"]:
                graph, input_graph = gen_small_graph_random_label(num_nodes, num_edges, test_type); #print(graph.nodes)
            # graph, input_graph = gen_small_graph_ring(num_nodes); 
        elif rand_flag == "enumerate":
            graph, input_graph = gen_small_graph_num_order(num_nodes, num_edges)
        elif rand_flag == "ring":
            if test_type in ["node_sum", "edge_sum", "mobility", "mobility_proj", "nonlinear", "nonlinear_relax", "therm_act"]:
                graph, input_graph = gen_small_graph_ring(num_nodes, test_type)
        # elif test_type in ["node_sum", "edge_sum", "mobility", "mobility_proj"]:
        #     graph, input_graph = gen_small_graph_random_label(num_nodes, num_edges, rand_flag)
        else:
            raise ValueError("Invalid value for rand_flag.")
        input_graphs.append(input_graph)
        graphs.append(graph)
    return graphs, input_graphs


def predict_and_plot(model, data, i, graph_name, model_version, output_dir=".", key="train"):
    output = model(data)
    predicted_values = output.cpu().detach().numpy()

    # R2_y1 = 2*data.x.cpu().numpy(); R2_y1 = R2_y1.flatten()# or data.y
    R2_y1 = data.y.cpu().detach().numpy(); R2_y1 = R2_y1.flatten()# or data.y
    R2_y2 = predicted_values.flatten()
    R2_val = r2_score(R2_y1, R2_y2); R2_val = round(R2_val,3)

    Pears_val = pearsonr(R2_y1, R2_y2)[0]; Pears_val = round(Pears_val,3)

    plt.figure(figsize=(5, 5))
    plt.scatter(R2_y1, R2_y2, label='Predicted', color='blue')
    plt.xlabel('Benchmark')
    plt.ylabel('Predicted')
    plt.xlim([500, 2500]);plt.ylim([500, 2500])
    plt.title(f'$R^2$: {R2_val}; Pearson: {Pears_val}')
    plt.legend(); plt.axis('equal')
    plt.savefig(f"{output_dir}/test_{i}_{key}.png"); 
    plt.close()

    plt.figure(figsize=(5, 3))
    plt.scatter(data.x.cpu().detach().numpy(), R2_y1, label='Benchmark', color='blue', alpha=0.5)
    plt.scatter(data.x.cpu().detach().numpy(), R2_y2, label='Prediction', color='red', alpha=0.5,marker='x') 
    plt.xlabel('$\epsilon$', fontsize=16); plt.ylabel('$\sigma$', fontsize=16); plt.legend(); plt.tight_layout()
    plt.savefig(f"{output_dir}/consti_{i}_{key}.png"); 
    plt.close()
    return R2_val, Pears_val


def predict_pend(model, data):
    output = model(data)
    predicted_values = output.cpu().detach().numpy()

    Xdat = data.x.cpu().detach().numpy(); Xdat = Xdat#.flatten()
    R2_y1 = data.y.cpu().detach().numpy(); R2_y1 = R2_y1#.flatten()# or 2*data.x data.y
    R2_y2 = predicted_values#.flatten()
    return R2_y2, R2_y1, Xdat


def stress_vonMises(stress):
    sig11 = stress[:,0]; sig12 = stress[:,1]; sig13 = stress[:,2]
    sig22 = stress[:,3]; sig23 = stress[:,4]; sig33 = stress[:,5]
    sig_vM = np.sqrt(.5*((sig11-sig22)**2 + (sig22-sig33)**2 + (sig33-sig11)**2 + 6*(sig23**2 + sig13**2 + sig12**2)))
    return sig_vM


def predict_overall(bench, input, pred, key, output_dir):
    bench = np.array(bench); pred = np.array(pred); input = np.array(input)
    eps11 = input[:,0]; sig11_bench = bench[:,0]; sig11_pred = pred[:,0];
    
    bench_mag = stress_vonMises(bench); bench_mag_single = bench_mag
    pred_mag = stress_vonMises(pred); pred_mag_single = pred_mag

    bench_mag = bench_mag[:, np.newaxis]  # Convert to column vector
    pred_mag = pred_mag[:, np.newaxis]    # Convert to column vector
    # input_mag = input_mag[:, np.newaxis] 

    bench_mag = np.repeat(bench_mag, 6, axis=1); bench_mag = bench_mag.flatten()
    pred_mag = np.repeat(pred_mag, 6, axis=1); pred_mag = pred_mag.flatten()
    # input_mag = np.repeat(input_mag, 6, axis=1); input_mag = input_mag.flatten()

    R2_y1 = bench; R2_y1 = R2_y1.flatten()# or data.y
    R2_y2 = pred.flatten(); input = input.flatten()
    R2_val = r2_score(R2_y1, R2_y2); R2_val = round(R2_val,3)

    Pears_val = pearsonr(R2_y1, R2_y2)[0]; Pears_val = round(Pears_val,3)

    plt.figure(figsize=(5, 5))
    plt.scatter(R2_y1, R2_y2, label='Predicted', color='blue', alpha=0.5)
    plt.plot([min(bench.flatten()), max(bench.flatten())],#min(bench)
             [min(bench.flatten()), max(bench.flatten())],
             linestyle='--', color='red', label='$y=x$')
    plt.xlabel('Benchmark', fontsize=15)
    plt.ylabel('Predictions', fontsize=15)
    plt.title(f'$R^2$: {R2_val}; Pearson: {Pears_val}', fontsize=16)
    plt.legend(); plt.axis('equal')
    # plt.xlim([500, 2500]);plt.ylim([500, 2500])
    plt.tight_layout()
    plt.savefig(f"{output_dir}/pred_overall_{key}", dpi=250); plt.close("all")

    x_min, x_max = input.min(), input.max()
    y_min, y_max = min(R2_y1.min(), R2_y2.min()), max(R2_y1.max(), R2_y2.max())

    '''overall constitutive model'''
    fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(12, 5))
    import matplotlib.colors as mcolors
    norm = mcolors.Normalize(vmin=min(bench_mag.min(), pred_mag.min()), vmax=max(bench_mag.max(), pred_mag.max()))

    # Plot the Benchmark scatter plot
    ax1.scatter(input, R2_y1, label='Benchmark', c=bench_mag, cmap='jet', norm=norm, alpha=0.5)# 
    ax1.set_xlabel('$\epsilon$', fontsize=16)
    ax1.set_ylabel('$\sigma$', fontsize=16)
    # ax1.legend()
    ax1.set_xlim(x_min, x_max)
    ax1.set_ylim(y_min, y_max)
    ax1.set_title('Benchmark', fontsize=16)
    
    sc2 = ax2.scatter(input, R2_y2, label='Predicted', c=pred_mag, cmap='jet', norm=norm, alpha=0.5)
    ax2.set_xlabel('$\epsilon$', fontsize=15)

    ax2.set_xlim(input.min(), input.max())
    ax2.set_ylim(y_min, y_max)
    ax2.set_title('Prediction', fontsize=16)
    # plt.close("all")# ax2.axis('equal')
    plt.subplots_adjust(right=0.85)
    cbar_ax = fig.add_axes([0.87, 0.15, 0.01, 0.7])
    cbar = fig.colorbar(sc2, cax=cbar_ax, orientation='vertical')
    cbar.set_label('von Mises stress', fontsize=15)

    plt.savefig(f"{output_dir}/consti_overall_{key}", dpi=250); 
    plt.close()

    '''error'''
    plt.figure(figsize=(2, 2))
    plt.hist(np.abs(R2_y1 - R2_y2), bins=256, alpha=0.75)
    plt.title('MAE', fontsize=16)
    plt.yscale("log")
    plt.tight_layout()
    plt.savefig(f"{output_dir}/MAE_overall_{key}", transparent=True); plt.close("all")
    
    '''1-direction'''
    fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(12, 5))
    norm = mcolors.Normalize(vmin=min(bench_mag.min(), pred_mag.min()), vmax=max(bench_mag.max(), pred_mag.max()))

    # Plot the Benchmark scatter plot
    ax1.scatter(eps11, sig11_bench, label='Benchmark', c=bench_mag_single, cmap='jet', norm=norm, alpha=0.5)# 
    ax1.set_xlabel('$\epsilon_{11}$', fontsize=16)
    ax1.set_ylabel('$\sigma_{11}$', fontsize=16)
    ax1.set_xlim(x_min, x_max)
    ax1.set_ylim(y_min, y_max)
    ax1.set_title('Benchmark', fontsize=16)
    
    sc2 = ax2.scatter(eps11, sig11_pred, label='Predicted', c=pred_mag_single, cmap='jet', norm=norm, alpha=0.5)
    ax2.set_xlabel('$\epsilon_{11}$', fontsize=16)
    ax2.set_xlim(input.min(), input.max())
    ax2.set_ylim(y_min, y_max)
    ax2.set_title('Prediction', fontsize=16)
    plt.subplots_adjust(right=0.85)
    cbar_ax = fig.add_axes([0.87, 0.15, 0.01, 0.7])
    cbar = fig.colorbar(sc2, cax=cbar_ax, orientation='vertical')
    cbar.set_label('von Mises stress', fontsize=15)

    plt.savefig(f"{output_dir}/dir11_overall_{key}", dpi=250); 
    plt.close()
    return R2_val, Pears_val


def prefactor_subgraph_index(subgindex_train, batch_train, active_index):
    poss = [(subgindex_train.flatten() == label) for label in active_index]

    if len(poss) == 1:
        merged_tensor = torch.logical_or(poss[0], torch.zeros_like(poss[0], dtype=torch.bool))#poss[0].tolist()
        filtered_indices = batch_train.edge_index.flatten()[merged_tensor]
        unique_indices = torch.unique(filtered_indices).tolist()
    elif len(poss) == 0:
        unique_indices = []
    else:
        merged_tensor = torch.logical_or(poss[0], poss[1])
        for tensor in poss[2:]:
            merged_tensor = torch.logical_or(merged_tensor, tensor)

        filtered_indices = batch_train.edge_index.flatten()[merged_tensor]
        unique_indices = torch.unique(filtered_indices).tolist()

    return unique_indices


def train_model(batch_overall, batch_active_ind, output_dir,\
                num_train_graphs, epochs, optimizer, criterion, model):
    loss_list = []
    for epoch in tqdm(range(epochs)):
        batch_epoch = batch_overall[epoch]
        active_ind = batch_active_ind[epoch]

        for num_train in range(num_train_graphs):
            batch_train = batch_epoch[num_train]
            active_index = active_ind[num_train]

            optimizer.zero_grad()
            gnn_pred = model(batch_train)
            if gnn_pred.shape == batch_train.y.shape:
                batch_train.y.requires_grad_()

                loss = criterion(gnn_pred[active_index], batch_train.y[active_index])
                if loss < 1e-6:
                    pass
                loss_list.append(loss.cpu().detach().numpy())
                loss.backward()
                torch.nn.utils.clip_grad_norm_(model.parameters(), max_norm=1.0)
                optimizer.step()
            else:
                pass
    plt.semilogy(np.array(loss_list), alpha=0.5)
    plt.xlabel('Itr.', fontsize=16); plt.ylabel('Loss', fontsize=16)
    plt.savefig(f"{output_dir}/loss_{epoch}", dpi=250)
    return model, loss_list


def train_model_full(batch_overall, output_dir, num_train_graphs, epochs, optimizer, criterion, model):
    loss_list = []
    for epoch in tqdm(range(epochs)):
        for num_train in range(num_train_graphs):
            batch_epoch = batch_overall[num_train]
            batch_train = batch_epoch.to(device)
            optimizer.zero_grad()
            gnn_pred = model(batch_train)
            if gnn_pred.shape == batch_train.y.shape:
                batch_train.y.requires_grad_()
                
                loss = criterion(gnn_pred, batch_train.y)
                if loss < 1e-6:
                    pass
                loss_list.append(loss.cpu().detach().numpy())
                loss.backward()
                torch.nn.utils.clip_grad_norm_(model.parameters(), max_norm=1.0)
                optimizer.step()
            else:
                pass
    plt.semilogy(np.array(loss_list))
    plt.xlabel('Epochs', fontsize=16); plt.ylabel('Loss', fontsize=16)
    plt.savefig(f"{output_dir}/loss_full_{epoch}", dpi=250)
    return model, loss_list


def test_model_accuracy(input_graphs, num_graphs, train_ratio, model, output_dir=".", train_size=0.8):
    test_graphs_ind = int(train_size * num_graphs)
    train_graphs_num = int(train_size * num_graphs) #72#len(input_graphs) #test_graphs_ind
    test_graphs_num = int((1-train_size) * num_graphs)
    test_graphs = input_graphs[test_graphs_ind:]
    train_graphs = input_graphs[:train_graphs_num]

    R2_list = []; Pearson_list = []
    pred_list = []; bench_list = []
    input_list = []
    
    for i in tqdm(range(test_graphs_ind)):
        batch = train_graphs[i]; 
        batch.to(device)
        R2_val, Pears_val = predict_and_plot(model, batch, i, input_graphs[i], i, output_dir, "train")
        pred_val, bench_val, input_val = predict_pend(model, batch)
        pred_list.append(pred_val); bench_list.append(bench_val)
        R2_list.append(R2_val); Pearson_list.append(Pears_val)
        input_list.append(input_val)

    print("train R2 scores:",R2_list)
    R2_val, Pears_val = predict_overall(np.concatenate(bench_list), np.concatenate(input_list), np.concatenate(pred_list), "train", output_dir)
    print("Overall train R2:",R2_val)
    R2_list = []; Pearson_list = []; pred_list = []; bench_list = []; input_list = []
    for i in tqdm(range(test_graphs_num)):
        
        batch = test_graphs[i]; batch.to(device)
        R2_val, Pears_val = predict_and_plot(model, batch, i, test_graphs[i], i, output_dir, "test")
        pred_val, bench_val, input_val = predict_pend(model, batch)
        pred_list.append(pred_val); bench_list.append(bench_val)
        R2_list.append(R2_val); Pearson_list.append(Pears_val)
        input_list.append(input_val)
    print("test R2 scores:",R2_list)
    '''overall test'''
    R2_val, Pears_val = predict_overall(np.concatenate(bench_list), np.concatenate(input_list), np.concatenate(pred_list), "test", output_dir)
    print("Overall test R2:",R2_val)
    return R2_list, bench_list, pred_list, R2_val

def test_model_overall(input_graphs, num_graphs, model, output_dir="."):
    train_graphs = input_graphs#[:test_graphs_ind]

    R2_list = []; Pearson_list = []
    pred_list = []; bench_list = []
    input_list = []
    
    for i in tqdm(range(int(num_graphs))):
        
        batch = train_graphs[i]; 
        batch.to(device)
        R2_val, Pears_val = predict_and_plot(model, batch, i, input_graphs[i], i, output_dir, "train")
        pred_val, bench_val, input_val = predict_pend(model, batch)
        pred_list.append(pred_val); bench_list.append(bench_val)
        R2_list.append(R2_val); Pearson_list.append(Pears_val)
        input_list.append(input_val)

    print("R2 scores:", R2_list)
    R2_val, Pears_val = predict_overall(np.concatenate(bench_list), np.concatenate(input_list), np.concatenate(pred_list), "train", output_dir)
    print("Overall R2:", R2_val)
    return R2_list, bench_list, pred_list, R2_val


def data_transform(data, train_idx):
    # mean_value = torch.mean(data.x);abs_mean_value = torch.abs(mean_value)
    abs_mean_value = torch.tensor([1e+9], dtype=data.x.dtype, device=data.x.device)
    pos_norm = torch.tensor([1e+2], dtype=data.x.dtype, device=data.x.device)
    data.x = data.x/abs_mean_value#torch.abs(torch.mean(data.x))
    data.y = data.y/abs_mean_value#torch.abs(torch.mean(data.x))

    return data, data.x, data.edge_index, data.edge_attr, data.y


def reorder_edge_index(edge_index):
    flat_edge_index = edge_index.flatten()
    unique_values, unique_indices = torch.unique(flat_edge_index, return_inverse=True)
    mapping = {old_idx.item(): new_idx.item() for old_idx, new_idx in zip(unique_values, torch.arange(unique_values.size(0)))}
    reordered_flat_edge_index = torch.tensor([mapping[idx.item()] for idx in flat_edge_index], dtype=torch.long)
    reordered_edge_index = reordered_flat_edge_index.view(edge_index.size())
    return reordered_edge_index


def batch_tranform(batch):
    batch.x = batch.x.to(torch.float)
    batch.edge_index = batch.edge_index.to(torch.long)
    batch.edge_attr = batch.edge_attr.to(torch.float)
    return batch


def graph_to_torch_tensor(graph):
    node_in = []; node_out = []
    for node, data in graph.nodes(data=True):
        in_features = data.get('F', [])
        node_in.append(in_features)
        out_features = data.get('v_avg', [])
        node_out.append(out_features)
    
    node_in = torch.tensor(node_in, dtype=torch.float)
    node_out = torch.tensor(node_out, dtype=torch.float)

    edge_indices = []; edge_features = []
    for u, v, data in graph.edges(data=True):
        edge_indices.append([u, v]); features = []
        plane_normal = data.get('plane_normal', [])
        link_len = data.get('link_len', None)
        
        if isinstance(plane_normal, (list, np.ndarray)):
            features.extend(plane_normal)
        if link_len is not None:
            features.append(link_len)
        edge_features.append(features)
    
    G_edges = [(edge[0], edge[1]) for edge in graph.edges()]
    edge_indices = torch.tensor([(edge[0][1], edge[1][1]) for edge in G_edges], dtype=torch.long).t().contiguous()
    edge_index_in_reordered = reorder_edge_index(edge_indices)
    edge_features = torch.tensor(edge_features, dtype=torch.float)

    # Create Data object
    data = Data(x=node_in, edge_index=edge_index_in_reordered, edge_attr=edge_features, y=node_out)

    return data